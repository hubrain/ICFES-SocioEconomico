import pandas as pd
import ppscore as pps
import matplotlib.pyplot as plt
import seaborn as sns
from dython.nominal import cluster_correlations
import os
from sklearn.impute import KNNImputer

data_dir = "../../../data/unziped_data/"
df = pd.read_csv(
    os.path.join(data_dir, "FTP_SABER11_20181.TXT"),
    sep="¬",
    header=0,
    encoding="utf-8-sig",
)
interesting = [
    "ESTU_NACIONALIDAD",
    "ESTU_GENERO",
    "ESTU_FECHANACIMIENTO",
    "FAMI_ESTRATOVIVIENDA",
    "FAMI_PERSONASHOGAR",
    "FAMI_CUARTOSHOGAR",
    "FAMI_EDUCACIONPADRE",
    "FAMI_EDUCACIONMADRE",
    "FAMI_TRABAJOLABORPADRE",
    "FAMI_TRABAJOLABORMADRE",
    "FAMI_TIENEINTERNET",
    "FAMI_TIENESERVICIOTV",
    "FAMI_TIENECOMPUTADOR",
    "FAMI_TIENELAVADORA",
    "FAMI_TIENEHORNOMICROOGAS",
    "FAMI_TIENEAUTOMOVIL",
    "FAMI_TIENEMOTOCICLETA",
    "FAMI_TIENECONSOLAVIDEOJUEGOS",
    "FAMI_NUMLIBROS",
    "FAMI_COMELECHEDERIVADOS",
    "FAMI_COMECARNEPESCADOHUEVO",
    "FAMI_COMECEREALFRUTOSLEGUMBRE",
    "FAMI_SITUACIONECONOMICA",
    "ESTU_DEDICACIONLECTURADIARIA",
    "ESTU_DEDICACIONINTERNET",
    "ESTU_HORASSEMANATRABAJA",
    "ESTU_TIPOREMUNERACION",
    "COLE_GENERO",
    "COLE_NATURALEZA",
    "COLE_CALENDARIO",
    "COLE_BILINGUE",
    "COLE_CARACTER",
    "COLE_JORNADA",
    "PERCENTIL_LECTURA_CRITICA",
    "PERCENTIL_MATEMATICAS",
    "PERCENTIL_C_NATURALES",
    "PERCENTIL_SOCIALES_CIUDADANAS",
    "PERCENTIL_INGLES",
    "PERCENTIL_GLOBAL",
    "ESTU_INSE_INDIVIDUAL",
    "ESTU_NSE_INDIVIDUAL",
    "ESTU_PILOPAGA",
]
print(len(interesting))

df_interesting = df[interesting]
keep = []
for column in df_interesting.columns:
    if len(df_interesting[column].unique()) < 40:
        if df_interesting[column].value_counts().min() >= 5:
            keep.append(column)
    else:
        keep.append(column)
df_interesting = df_interesting[keep]
# imputer = KNNImputer()
# data = imputer.fit_transform(df_interesting.to_numpy())
# df_interesting = pd.DataFrame(data=data, columns=df_interesting.columns)

print('Total data is', df_interesting.shape[0])

sample_size = min(100000, df_interesting.shape[0])

df_matrix = pps.matrix(df_interesting, sample=sample_size, random_seed=0)[
    ["x", "y", "ppscore"]
].pivot(columns="x", index="y", values="ppscore")
pps = df_matrix.round(2)

print(pps.columns)

keep_cols = []
threshold = 0.3
for column in pps.columns:
    fill_row = ((pps.loc[column, :] >= threshold) & ((pps.loc[column, :] != 1))).any()
    fill_column = (
        (pps.loc[:, column] >= threshold) & ((pps.loc[:, column] != 1))
    ).any()
    if fill_row == True and fill_column == fill_row:
        keep_cols.append(column)

print(len(keep_cols))

correlations, _ = cluster_correlations(pps.loc[keep_cols, keep_cols])

fig, ax = plt.subplots(figsize=(20, 15))
sns.heatmap(
    correlations, vmin=0, vmax=1, cmap="Blues", linewidths=0.5, annot=True, ax=ax
)
plt.savefig("predictive_score_matrix.pdf", bbox_inches="tight", dpi=300)
plt.show()
