import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import pyplot
import os
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import xgboost as xgb
import joblib
import shap
import sys

sys.path.append('../../')
from utils import *

# Create the directory to save the results
folder = '../shapsummary/'
if not os.path.exists(folder):
    os.makedirs(folder)


def save_plots(name): 
    name += '.png'
    f = plt.gcf()
    f.tight_layout()
    plt.savefig(name, dpi=600, bbox_inches="tight")
    plt.clf()


def shap_summary_plots(year, periodo):
    # Data directory
    data_dir = "../../../data/unziped_data/"

    # Open data parsing with CSV
    data_for_training = "../../../data/Data_For_Training/"

    # Read data_training
    df_training = pd.read_csv(data_for_training + "icfes_{0}{1}.csv".format(year, periodo))

    # lista de las columnas con las caracteristicas a predecir
    notasz = [c for c in df_training.columns.values if c.startswith("NOTA_Z")]
    deciles = [c for c in df_training.columns.values if c.startswith("DECILE")]
    labels_names = notasz + deciles

    # lista de las columnas que se van a omitir de las caracteristicas
    vars_2_omit = (
            [
                "PUNT_LECTURA_CRITICA",
                "PERCENTIL_LECTURA_CRITICA",
                "DESEMP_LECTURA_CRITICA",
                "PUNT_MATEMATICAS",
                "PERCENTIL_MATEMATICAS",
                "DESEMP_MATEMATICAS",
                "PUNT_C_NATURALES",
                "PERCENTIL_C_NATURALES",
                "DESEMP_C_NATURALES",
                "PUNT_SOCIALES_CIUDADANAS",
                "PERCENTIL_SOCIALES_CIUDADANAS",
                "DESEMP_SOCIALES_CIUDADANAS",
                "PUNT_INGLES",
                "PERCENTIL_INGLES",
                "DESEMP_INGLES",
                "PUNT_GLOBAL",
                "PERCENTIL_GLOBAL",
            ]
            + deciles
            + notasz
    )

    labels = df_training[labels_names]
    features = df_training.drop(vars_2_omit, axis=1)

    scaler_features = joblib.load("../models/scaler_features_20181.gz")

    new_features = scaler_features.transform(features)
    features = pd.DataFrame(data=new_features, columns=features.columns)
    mask_nota_z = [False if 'COLE' in col or 'DEPTO' in col or 'MCPIO' in col or "ESTABLECIMIENTO" in col else True for col in features.columns]

    for ln in notasz:
        print(ln)
        model = upload_xgb_model(ln, year, periodo)

        # Shap implementation of model
        explainer = shap.TreeExplainer(model)

        # Upload data that model use for training
        shap_values = explainer.shap_values(features.iloc[:, mask_nota_z])

        # shap.force_plot(explainer.expected_value, shap_values[0,:], features.iloc[0,:])

        shap.summary_plot(shap_values, features.iloc[:, mask_nota_z], show=False)
        file_path = folder + ln + 'impact_new'
        save_plots(file_path)

    for ln in deciles:
        print(ln)
        model = upload_xgb_model(ln, year, periodo)

        # Shap implementation of model
        explainer = shap.TreeExplainer(model)

        # Upload data that model use for training
        shap_values = explainer.shap_values(features)

        # shap.force_plot(explainer.expected_value, shap_values[0,:], features.iloc[0,:])

        shap.summary_plot(shap_values, features, show=False)
        file_path = folder + ln + 'impact_new'
        save_plots(file_path)


#
shap_summary_plots(2018, 1)
