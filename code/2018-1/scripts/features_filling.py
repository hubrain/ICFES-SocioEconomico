import pandas as pd
import sys
from sklearn.impute import IterativeImputer, KNNImputer

# Open data pandas read csv
data_for_training = "../../../data/Data_For_Training/"
data = pd.read_csv(data_for_training + "icfes_20181.csv")
data = data.set_index("Unnamed: 0")

porcentaje = 0.35
thresh = int(data.shape[0] * porcentaje)
print("filas totales", data.shape[0], " requerimos ", thresh, "filas llenas")
print(
    "cantidad de columnas ",
    len(
        set(data.columns.to_list()).difference(
            data.dropna(axis="columns", thresh=thresh).columns.to_list()
        )
    ),
)
low_filled_2 = list(
    set(data.columns.to_list()).difference(
        data.dropna(axis="columns", thresh=thresh).columns.to_list()
    )
)

names_nota_z = [col for col in data.columns if "NOTA_Z" in col]
df_nn = data.drop(low_filled_2, axis=1)
print("Incluir Nota Z con valores NaN")
print("Cantidad de columnas ", df_nn.shape[1], ", Cantidad de filas", df_nn.shape[0])
print("Eliminar Nota Z con valores NaN")
df_nn = df_nn.dropna(subset=names_nota_z)
print("Cantidad de columnas ", df_nn.shape[1], ", Cantidad de filas", df_nn.shape[0])

# Llenar los valores incompletos
print("Llenando los dataframes")

import impyute as impy

sys.setrecursionlimit(100000)
values = df_nn.to_numpy()

print("Using the EM method")

data_em = impy.em(values)
df_nn_em = pd.DataFrame(data=data_em, columns=df_nn.columns.to_list())
df_nn_em.set_index(df_nn.index)
df_nn_em.to_csv(data_for_training + "icfes_20181_nn_em.csv")

print("Using the MICE method")

imp_mean = IterativeImputer(random_state=0)
data_mice = imp_mean.fit_transform(values)
df_nn_mice = pd.DataFrame(data=data_mice, columns=df_nn.columns.to_list())
df_nn_mice.set_index(df_nn.index)
df_nn_mice.to_csv(data_for_training + "icfes_20181_nn_mice.csv")

print("Using the knn method")

knn_imputer = KNNImputer(n_neighbors=5)
data_knn = knn_imputer.fit_transform(values)
df_nn_knn = pd.DataFrame(data=data_knn, columns=df_nn.columns.to_list())
df_nn_knn.set_index(df_nn.index)
df_nn_knn.to_csv(data_for_training + "icfes_20181_nn_knn.csv")
