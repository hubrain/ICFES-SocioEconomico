import pandas as pd
import numpy as np
import os
from keras.models import Sequential
from keras.layers import Dense
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
import optuna
import pickle
import json
from functools import partial
import tensorflow as tf

def create_test_data(num_samples, num_features, labels):
    from sklearn.datasets import make_regression
    X, Y = make_regression(num_samples, num_features, n_targets=len(labels), random_state=0)
    feats_labels = [f"feature_{i}" for i in range(num_features)]
    data = np.hstack((X, Y))
    all_labels = feats_labels + labels
    df = pd.DataFrame(data=data, columns=all_labels)
    size = df.shape[0]
    cut = int(size*0.8)
    dftrain = df.iloc[:cut, :]
    dftest = df.iloc[cut:, :]
    return dftrain, dftest


def pre_objective(trial,X_train,Y_train,X_test,Y_test):
    '''
    Functions to have the best hyperparameters
    '''

    activation = trial.suggest_categorical('activation', ['sigmoid','softplus','tanh','relu'])

    num_neu_layer1 = trial.suggest_int("num_neu_layer1", 1, 40)
    num_neu_layer2 = trial.suggest_int("num_neu_layer2", 1, 30)
    epochs = trial.suggest_int("epochs", 1, 100)
    batch_size = trial.suggest_int("batch_size",8,512)


    try:
        #Create squential
        model = Sequential()
        model.add(Dense(num_neu_layer1, input_dim=X_train.shape[1] , activation=activation))
        model.add(Dense(num_neu_layer2, activation=activation))
        model.add(Dense(1, activation='linear')) 
        model.compile(loss='mean_squared_error',optimizer='adam')
        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
        model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size, callbacks=[callback], verbose=False)
        
        #Model predictions
        pred_train = model.predict(X_train)
        pred_test = model.predict(X_test)

        #error to minmize
        to_minimize_1 = mean_squared_error(pred_test, Y_test, squared=False)
        to_minimize_2 = mean_squared_error(pred_train, Y_train, squared=False)
        print("Error in test: ", to_minimize_1, ", Error in train: ", to_minimize_2)
        err = to_minimize_1 + to_minimize_2 + abs(to_minimize_1 - to_minimize_2)
    except Exception as e:
        print(e)
        print('Error')
        err = np.nan
    
    return err

def tune_classifier(X_train,Y_train,X_test,Y_test, n_trials, name):
    objective = partial(pre_objective, X_train=X_train,Y_train=Y_train,X_test=X_test,Y_test=Y_test)
    study = optuna.create_study(
        study_name="icfes_20181_{0}_nn".format(name),
        storage="sqlite:///../models/icfes_20181_{0}_nn.db".format(name),
    )
    study.optimize(objective, n_trials=n_trials)
    return study.best_params


def test():
    # Data directory
    data_dir = "../../../data/unziped_data/"

    # Open data pandas read csv
    data_for_training = "../../../data/Data_For_Training/"
    data = pd.read_csv(data_for_training + "icfes_20181.csv")  # THIS IS AN EXAMPLE, HAS TO LOAD JUAN' RESULTS

    notasz = [c for c in data.columns.values if c.startswith("NOTA_Z")]
    deciles = [c for c in data.columns.values if c.startswith("DECILE")]

    # lista de las columnas con las caracteristicas a predecir
    labels_names = notasz + deciles

    # now we create test data while we wait for Juan's results:

    data_train, data_test = create_test_data(10000, 30, labels_names)

    features_train = data_train.drop(labels_names, axis=1)
    features_test = data_test.drop(labels_names, axis=1)
    labels_train = data_train[labels_names]
    labels_test = data_test[labels_names]

    models_files = os.listdir("../models/")

    if "scaler_features_umap_20181.pkl" not in models_files:
        scaler_features = StandardScaler()
        scaler_features.fit(features_train)
        with open("../models/scaler_features_umap_20181.pkl", "wb") as f:
            pickle.dump(scaler_features, f)
    else:
        with open("../models/scaler_features_umap_20181.pkl", "rb") as f:
            scaler_features = pickle.load(f)

    if "scaler_labels_umap_20181.pkl" not in models_files:
        scaler_labels = StandardScaler()
        scaler_labels.fit(labels_train)
        with open("../models/scaler_labels_umap_20181.pkl", "wb") as f:
            pickle.dump(scaler_labels, f)
    else:
        with open("../models/scaler_labels_umap_20181.pkl", "rb") as f:
            scaler_labels = pickle.load(f)

    X_train = scaler_features.transform(features_train)
    X_test = scaler_features.transform(features_test)
    Y_train = scaler_labels.transform(labels_train)
    Y_test = scaler_labels.transform(labels_test)

    for ii in range(len(labels_names)):
        if f"best_params_20181_nn_{labels_names[ii]}.json" in models_files:
            continue
        y_train = Y_train[:, ii]
        y_test = Y_test[:, ii]
        best_params = tune_classifier(X_train, y_train, X_test, y_test, 10, labels_names[ii])
        with open(f"../models/best_params_20181_nn_{labels_names[ii]}.json", "w") as f:
            json.dump(best_params, f)

def load_crude_reduced_data(dir_data_for_training, column_predict,verbose=True):
    """
    dir_data_for_training is path from this file's directory, with trailing /
    column_predict is one of the columns to be predicted
    """
    df_train = pd.read_csv(dir_data_for_training + f"2018_1_nn_train_{column_predict}.csv")
    df_test = pd.read_csv(dir_data_for_training + f"2018_1_nn_test_{column_predict}.csv")
    df_train = df_train.dropna()
    df_test = df_test.dropna()

    features_train = df_train[[column for column in df_train.columns if 'feature' in column]]
    features_test = df_test[[column for column in df_test.columns if 'feature' in column]]
    labels_train = df_train[[column_predict]]  # double brackets to return DataFrame
    labels_test = df_test[[column_predict]]
    if verbose:
        print(column_predict, df_train.isna().sum())
        print(column_predict, features_train.isna().sum())
    return features_train, labels_train, features_test, labels_test

def scale_reduced_data(models_files, column_predict, features_train, labels_train, features_test, labels_test):
    if f"scaler_features_umap_20181_{column_predict}.pkl" not in models_files:
        scaler_features = StandardScaler()
        scaler_features.fit(features_train)
        with open(f"../models/scaler_features_umap_20181_{column_predict}.pkl", "wb") as f:
            pickle.dump(scaler_features, f)
    else:
        with open(f"../models/scaler_features_umap_20181_{column_predict}.pkl", "rb") as f:
            scaler_features = pickle.load(f)

    if f"scaler_labels_umap_20181_{column_predict}.pkl" not in models_files:
        scaler_labels = StandardScaler()
        scaler_labels.fit(labels_train)
        with open(f"../models/scaler_labels_umap_20181_{column_predict}.pkl", "wb") as f:
            pickle.dump(scaler_labels, f)
    else:
        with open(f"../models/scaler_labels_umap_20181_{column_predict}.pkl", "rb") as f:
            scaler_labels = pickle.load(f)

    X_train = scaler_features.transform(features_train)
    X_test = scaler_features.transform(features_test)
    y_train = scaler_labels.transform(labels_train).flatten()
    y_test = scaler_labels.transform(labels_test).flatten()

    return X_train, y_train, X_test, y_test

def main(n_trials=10):
    dir_data_for_training = "../../../data/Data_For_Training/"
    data = pd.read_csv(dir_data_for_training + "icfes_20181.csv", nrows=1)
    notasz = [c for c in data.columns.values if c.startswith("NOTA_Z")]
    deciles = [c for c in data.columns.values if c.startswith("DECILE")]
    models_files = os.listdir("../models/")
    resultados_2_fit = notasz + deciles

    for column in resultados_2_fit:
        features_train, labels_train, features_test, labels_test = load_crude_reduced_data(dir_data_for_training, column)
        X_train, y_train, X_test, y_test = scale_reduced_data(models_files, column, 
            features_train.to_numpy(), labels_train.to_numpy(), features_test.to_numpy(), labels_test.to_numpy())
        print(column, X_train.shape, y_train.shape)
        if f"best_params_20181_nn_{column}.json" in models_files:
            print(f"best_params_20181_nn_{column}.json already exists.")
            continue
        print(np.isnan(X_train).sum())
        best_params = tune_classifier(X_train, y_train, X_test, y_test, n_trials, column)
        with open(f"../models/best_params_20181_nn_{column}.json", "w") as f:
            json.dump(best_params, f)

        X = np.vstack((X_train, X_test))
        y = np.hstack((y_train, y_test))


        model = Sequential()
        model.add(Dense(best_params['num_neu_layer1'], input_dim=X.shape[1] , activation=best_params['activation']))
        model.add(Dense(best_params['num_neu_layer2'], activation=best_params['activation']))
        model.add(Dense(1, activation='linear')) 
        model.compile(loss='mean_squared_error',optimizer='adam')
        
        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
        
        model.fit(X, y, epochs=best_params['epochs'], batch_size=best_params['batch_size'], callbacks=[callback])
        
        model.save(f"../models/nn_20181_{column}.h5")

if __name__ == '__main__':
    main(n_trials=200)