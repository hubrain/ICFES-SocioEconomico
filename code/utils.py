# import random
# import itertools
import numpy as np
from sklearn.metrics import accuracy_score
from scipy.stats import norm
import xgboost as xgb
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import hashlib

def sha256sum(filename):
    """
    Computes the SHA256 digest of a file
    
    Parameters
    ----------
    filename: str
        Path to a file

    Returns
    -------
    str
        SHA256 of the file at `filename`
    """
    BUF_SIZE = 65536
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha256.update(data)

    return sha256.hexdigest()

def scatter(true, pred,which,y_label='Predicted score',x_label='True score',path='example.png'):
    '''
    Parameters
    ----------
    true: np.ndarray
        Array with true results 
    pred: np.ndarray 
        Array with predict results
    which: list[boolean]
        List of booleans with `True`to possitive result and `False` to negative result
    y_label: str
        Label to axis y
    x_label: str
        Label to axis x
    path: str
        Path to save the scatter plot `example.png`
    '''
    which = list(map(lambda x: 'Positve' if x else 'Negative', which))
    df = pd.DataFrame({x_label:true,y_label:pred,'Result':which})
    plot_neg = sns.scatterplot(data=df[df.Result == 'Negative'], x=x_label, y=y_label,hue='Result',s=8,linewidth=0,alpha=0.005,palette=['Blue'])
    plot_pos = sns.scatterplot(data=df[df.Result == 'Positve'], x=x_label, y=y_label,hue='Result',s=8,alpha=0.1,linewidth=0,palette=['Red'])
    plot_neg.plot
    plot_pos.plot
    plt.savefig(path, bbox_inches='tight', dpi=400)
    plt.close()

def compare_pair(true_labels, predicted_labels, uniform=True, num_samples=20000):
    """
    Function that takes the true labels and predicted labels and checks if relative order
    is conserved
    @param true_labels: numpy.array. True labels (scores)
    @param predicted_labels: numpy.array. Predicted labels (scores)
    @return float. Accuracy of keeping the order
    """
    num_instances = true_labels.size
    if uniform:
        # combs = itertools.combinations(range(num_instances), 2)  # this can be 2 big
        pairs = np.array([np.random.choice(num_instances, 2, replace=False) for x in range(num_samples)])
        col1 = pairs[:, 0]
        col2 = pairs[:, 1]
        diffs_true = np.sign(true_labels[col1] - true_labels[col2])
        diffs_pred = np.sign(predicted_labels[col1] - predicted_labels[col2])
        return accuracy_score(diffs_pred, diffs_true)
    else:
        # Sample pairs whose true labels are close to one another with a gaussian distribution
        # First order true labels:
        idx = np.argsort(true_labels)
        singles = np.random.choice(num_instances, num_samples)
        gaussian = norm.ppf(np.random.random(num_samples), loc=0, scale=5).astype(int)
        gaussian[gaussian == 0] = np.random.choice([-1, 1], (gaussian == 0).sum())
        second = singles + gaussian
        second[second < 0] = - second[second < 0] + 1
        second[second >= num_instances] = 2*num_instances - second[second >= num_instances] - 1
        second[second < 0] = 1
        second[second >= (num_instances - 1)] = num_instances - 2
        second[second == singles] = second[second == singles] + np.random.choice([-1, 1], (second == singles).sum())
        pairs = np.vstack((singles, second)).T
        col1 = pairs[:, 0]
        col2 = pairs[:, 1]
        diffs_true = np.sign(true_labels[idx][col1] - true_labels[idx][col2])
        diffs_pred = np.sign(predicted_labels[idx][col1] - predicted_labels[idx][col2])
        return accuracy_score(diffs_pred, diffs_true)

def upload_xgb_model(ln , year, periodo):
    '''Function to upload a xgboost model'''
    fname = "../models/icfes_{0}{1}_".format(year,periodo) + ln + "_v2.model"
    bst = xgb.Booster({'nthread': 4})
    bst.load_model(fname)
    if xgb.__version__ == '1.1.1':
        model_bytearray = bst.save_raw()[4:]
        def myfun(self=None):
            return model_bytearray

        bst.save_raw = myfun
    return bst

def Calculate_and_sort_scores(ln, features,year,periodo):
    ''' Calculated and sort scores of a model '''
    fname = "models/icfes_{0}{1}_".format(year,periodo) + ln + ".model"
    bst = xgb.Booster({'nthread': 4})
    bst.load_model(fname)
    score = bst.get_score(importance_type="total_gain")
    dict_1 = {features.columns[int(k.replace("f",""))]: v for k, v in sorted(score.items(), key=lambda item: item[1], reverse=True)}
    return dict_1

if __name__ == "__main__":
    print("Test compare pair")
    labels1 = np.random.randn(100)
    labels2 = labels1 + np.random.randn(100)/50
    print(compare_pair(labels1, labels2, uniform=True, num_samples=200))
    print(compare_pair(labels1, labels2, uniform=False, num_samples=200))
