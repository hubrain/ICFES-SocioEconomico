"""
This file is used to generate frequency distributions of those students selected as outstanding vs non-outstanding.
"""

import pandas as pd
from matplotlib import pyplot as plt
import os
import seaborn as sns

comparison_type = "group"
print(
    f"You are printing distributions for the comparison type '{comparison_type}'.\n"
    "If you want another one, please change the comparison_type variable"
)

data_for_training = "../../../data/Data_For_Training/"
data_complete = pd.read_csv(data_for_training + "icfes_20182_complete.csv", index_col=0)
df = pd.read_csv(
    f"../outliers/{comparison_type}_comparison_outliers.csv.gz",
    compression="gzip",
    index_col=0,
)
sobresalientes = data_complete.loc[df.query("which == True").index]

resto = data_complete.loc[df.query("which == False").index]

columns_with_get_dummies = [
    "ESTU_DEPTO_RESIDE",
    "COLE_GENERO",
    "COLE_CALENDARIO",
    "COLE_CARACTER",
    "COLE_JORNADA",
    "COLE_DEPTO_UBICACION",
    "ESTU_DEPTO_PRESENTACION",
    "ESTU_TIPOREMUNERACION",
]
# lista de las columnas que se van a convertir en float
numeric_var = [
    "PUNT_LECTURA_CRITICA",
    "PERCENTIL_LECTURA_CRITICA",
    "DESEMP_LECTURA_CRITICA",
    "PUNT_MATEMATICAS",
    "PERCENTIL_MATEMATICAS",
    "DESEMP_MATEMATICAS",
    "PUNT_C_NATURALES",
    "PERCENTIL_C_NATURALES",
    "DESEMP_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "PERCENTIL_SOCIALES_CIUDADANAS",
    "DESEMP_SOCIALES_CIUDADANAS",
    "PUNT_INGLES",
    "PERCENTIL_INGLES",
    "PUNT_GLOBAL",
    "PERCENTIL_GLOBAL",
    "ESTU_INSE_INDIVIDUAL",
    "ESTU_NSE_ESTABLECIMIENTO",
]
# lista de la columnas a las que toca hacerles funciones de parsing
for_parsing = [
    "DESEMP_INGLES",
    "ESTU_NSE_INDIVIDUAL",
    "ESTU_NACIONALIDAD",
    "ESTU_GENERO",
    "ESTU_PAIS_RESIDE",
    "ESTU_ETNIA",
    "ESTU_LIMITA_MOTRIZ",
    "FAMI_ESTRATOVIVIENDA",
    "FAMI_PERSONASHOGAR",
    "FAMI_CUARTOSHOGAR",
    "FAMI_TIENEINTERNET",
    "FAMI_TIENESERVICIOTV",
    "FAMI_TIENECOMPUTADOR",
    "FAMI_TIENELAVADORA",
    "FAMI_TIENEHORNOMICROOGAS",
    "FAMI_TIENEAUTOMOVIL",
    "FAMI_TIENEMOTOCICLETA",
    "FAMI_TIENECONSOLAVIDEOJUEGOS",
    "FAMI_NUMLIBROS",
    "FAMI_COMELECHEDERIVADOS",
    "FAMI_COMECARNEPESCADOHUEVO",
    "FAMI_COMECEREALFRUTOSLEGUMBRE",
    "FAMI_SITUACIONECONOMICA",
    "FAMI_TRABAJOLABORMADRE",
    "FAMI_TRABAJOLABORPADRE",
    "FAMI_EDUCACIONMADRE",
    "FAMI_EDUCACIONPADRE",
    "ESTU_DEDICACIONLECTURADIARIA",
    "ESTU_DEDICACIONINTERNET",
    "ESTU_HORASSEMANATRABAJA",
    "COLE_NATURALEZA",
    "COLE_BILINGUE",
    "COLE_SEDE_PRINCIPAL",
    "COLE_AREA_UBICACION",
    "ESTU_PRIVADO_LIBERTAD",
]

path = f"../outliers/feature_histograms_with_{comparison_type}_comparison"
try:
    os.mkdir(path)
except:
    pass
try:
    os.mkdir(path + "/no_aggregation")
except:
    pass
columns = columns_with_get_dummies + for_parsing
columns_hist = [col + "_original" for col in columns]
lenght = len(columns_hist)
for colum in columns_hist:
    lenvals = data_complete[colum].unique().size
    fig, axs = plt.subplots(1, 2, figsize=(12, 1.5 * lenvals), sharey=True)
    labels_sobre = (
        sobresalientes[colum].value_counts(normalize=True).sort_index().index.values
    )
    counts_sobre = (
        sobresalientes[colum].value_counts(normalize=True).sort_index().values
    ) * 100
    labels_resto = resto[colum].value_counts(normalize=True).sort_index().index.values
    counts_resto = resto[colum].value_counts(normalize=True).sort_index().values * 100

    axs[0].barh(
        labels_sobre, counts_sobre, color="red", alpha=0.5, label="Sobresalientes"
    )
    axs[0].barh(labels_resto, counts_resto, color="blue", alpha=0.5, label="Resto")
    axs[0].legend(loc="upper right")

    labels_difer = (
        (
            resto[colum].value_counts(normalize=True)
            - sobresalientes[colum].value_counts(normalize=True)
        )
        .sort_index()
        .index.values
    )
    counts_difer = (
        (
            resto[colum].value_counts(normalize=True)
            - sobresalientes[colum].value_counts(normalize=True)
        )
        .sort_index()
        .values
    ) * 100
    axs[0].tick_params(axis="x", rotation=90)
    axs[1].barh(labels_difer, counts_difer, color="green", label="diferencia (%)")
    axs[1].tick_params(axis="x", rotation=90)
    axs[1].legend(loc="upper right")
    plt.savefig(
        path
        + "/no_aggregation"
        + "/distribution_{0}.pdf".format(colum.replace("_original", "")),
        bbox_inches="tight",
        dpi=300,
    )
    plt.close()


def bar_plot(df, column, group, ax):

    df = df.dropna(subset=[group])
    grouped = df.groupby([column], sort=False)
    try:
        occupation_counts = grouped[group].value_counts(normalize=True, sort=False)
    except IndexError:
        print(f"column {column} is failing when computing value_counts")
        return False

    occupation_data = [
        {column: occupation, group: income, "percentage": percentage}
        for (occupation, income), percentage in occupation_counts.to_dict().items()
    ]

    labels = list(resto[column].dropna().unique())
    labels_group = list(resto[group].dropna().unique())
    df_occupation = pd.DataFrame(occupation_data)

    sns.barplot(
        ax=ax,
        y=column,
        x="percentage",
        hue=group,
        hue_order=labels_group,
        data=df_occupation,
        order=labels,
    )
    return True


def grouped_chart(path_group, group="ESTU_GENERO_original"):
    columns = columns_with_get_dummies + for_parsing
    columns_hist = [col + "_original" for col in columns]
    for colum in columns_hist:

        lenvals = data_complete[colum].unique().size
        fig, axs = plt.subplots(1, 2, figsize=(12, 1.5 * lenvals), sharey=True)

        if not bar_plot(sobresalientes, colum, group, axs[0]):
            continue
        axs[0].set_title("Sobresalientes")

        if not bar_plot(resto, colum, group, axs[1]):
            continue
        axs[1].set_title("Resto")

        plt.savefig(
            path_group
            + "/dis_by_{1}__{0}.pdf".format(colum.replace("_original", ""), group),
            bbox_inches="tight",
        )
        plt.close()


try:
    os.mkdir(path + "/gender_aggregated")
except:
    pass
grouped_chart(path + "/gender_aggregated", group="ESTU_GENERO_original")

try:
    os.mkdir(path + "/nse_aggregated")
except:
    pass
grouped_chart(path + "/nse_aggregated", group="ESTU_NSE_INDIVIDUAL_original")
