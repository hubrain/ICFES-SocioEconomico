import pandas as pd
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from functools import partial
from sklearn.metrics import confusion_matrix, roc_curve, auc
import json
import joblib
import optuna
from functools import partial
from sklearn.metrics import mean_squared_error

# Data directory
data_dir = "../../../data/unziped_data/"

# Open data with pickle
data_for_training = "../../../data/Data_For_Training/"
data = pd.read_csv(data_for_training + "icfes_20182.csv")

# lista de las columnas con las caracteristicas a predecir
notasz = [c for c in data.columns.values if c.startswith("NOTA_Z")]
deciles = [c for c in data.columns.values if c.startswith("DECILE")]
labels_names = notasz + deciles

# lista de las columnas que se van a omitir de las caracteristicas
vars_2_omit = (
    [
     'PUNT_LECTURA_CRITICA', 
     'PERCENTIL_LECTURA_CRITICA', 
     'DESEMP_LECTURA_CRITICA', 
     'PUNT_MATEMATICAS', 
     'PERCENTIL_MATEMATICAS', 
     'DESEMP_MATEMATICAS', 
     'PUNT_C_NATURALES', 
     'PERCENTIL_C_NATURALES', 
     'DESEMP_C_NATURALES', 
     'PUNT_SOCIALES_CIUDADANAS', 
     'PERCENTIL_SOCIALES_CIUDADANAS', 
     'DESEMP_SOCIALES_CIUDADANAS', 
     'PUNT_INGLES', 
     'PERCENTIL_INGLES', 
     'DESEMP_INGLES', 
     'PUNT_GLOBAL', 
     'PERCENTIL_GLOBAL', 
    ]
    + deciles
    + notasz
)

labels = data[labels_names]
features = data.drop(vars_2_omit, axis=1)

scaler_features = StandardScaler()
scaler_features.fit(features)
scaler_labels = StandardScaler()
scaler_labels.fit(labels)

joblib.dump(scaler_features, "../models/scaler_features_20182.gz")
joblib.dump(scaler_labels, "../models/scaler_labels_20182.gz")

X = scaler_features.transform(features)
y = scaler_labels.transform(labels)

for ii, label_name in enumerate(labels_names):
    with open(
        "../models/best_params_20182_xgboost_{0}.json".format(labels_names[ii]), "r"
    ) as f:
        Final_parameters = json.load(f)

    num_round = Final_parameters.pop("num_round")

    Final_parameters.update(
        {
            "verbosity": 1,
            "nthread": 4,
            "random_state": 0,
            "objective": "reg:squarederror",
            "booster": "gbtree",
            "tree_method": "hist",
            "eval_metric": "rmse",
        }
    )
    idx = np.where(~np.isnan(y[:, ii]))
    dtrain = xgb.DMatrix(X[idx[0]], label=y[:, ii][idx[0]])

    bst = xgb.train(
        Final_parameters,
        dtrain,
        num_round,
        evals=[(dtrain, "train")],
        early_stopping_rounds=10,
        verbose_eval=True,
    )

    ###Save Complete model
    bst.save_model("../models/icfes_20182_{0}_v2.model".format(label_name))
