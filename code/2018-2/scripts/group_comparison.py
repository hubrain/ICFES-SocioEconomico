"""
Run create_complete_data before running this code
"""

import pandas as pd
import numpy as np
from sklearn.cluster import KMeans as skl_KMeans
from matplotlib import pyplot as plt
from tqdm import tqdm
from local_comparison import is_outstanding
import sys
sys.path.append("../..")
from utils import sha256sum

# Generación E number of elegible people are in https://gitlab.com/hubrain/ICFES-SocioEconomico/-/merge_requests/49
num_genE = 3737 + 100

### Functions
def gap_statistics(
    vectors,
    n_refs=6,
    min_clusters=1,
    max_clusters=15,
    step_clusters=1,
    kmeans_params={},
    verbose=False,
    show=True,
):
    """
    Compute the gap-statistics for a kmeans clustering

    Params
    ------
    vectors: numpy ndarray of shape (N,M)
        Vectors to cluster. N is the number of vectors and M their dimension.
    n_refs: int
        Number of random references used to compute the gap-statistics
    min_clusters: int
        Minimum number of clusters to fit
    max_clusters: int
        Maximum number of clusters to fit
    step_clusters: int
        Step used to increment the amount of clusters to fit
    kmeans_params: dict
        Dictionary containing the parameters of the kmeans clustering algorithm
    verbose: bool
        Whether to print information about the process or not
    show: bool
        Whether to show the gap-statistics figure or not

    Returns
    -------
    n_clusters: {int, str}
        If show then n_clusters is the optimal number of clusters, else, a string
        saying the is necessary to look at the plot to find the optimal number of
        clusters.
    """

    if isinstance(vectors, list):
        X = np.vstack(vectors)
    else:
        X = vectors
    if X.ndim != 2:
        raise ValueError(
            "The array containing the data does not have two (2) dimensions, it has {} dimensions".format(
                X.ndim
            )
        )

    # Scale the data so that each column vector is in the range [0,1) to obtain
    # a better comparison when calculating the gap statistics
    try:
        scale_factor = 1.0 / np.max(np.max(X, axis=0) - np.min(X, axis=0))
    except:
        scale_factor = 1.0

    X = (X - np.min(X, axis=0)) * scale_factor

    gap_measurements = []
    gap_std = []
    if verbose:
        print("min_clusters: ", min_clusters, ", max_clusters: ", max_clusters)
        print("n_refs: ", n_refs)
    for k in range(min_clusters, max_clusters + 1, step_clusters):
        ref_disp = np.zeros(n_refs)
        for i in range(n_refs):
            if verbose:
                print("Gap_statistics, clusters: ", k, ", referencia: ", i)
            # Create a random reference set
            random_reference = np.random.random_sample(size=X.shape)
            # Fit k clusters to the the random reference set
            kmeans_params["n_clusters"] = k
            kmeans = skl_KMeans(**kmeans_params)
            kmeans.fit(random_reference)
            # Save the sum of squared distances of samples to their closest cluster
            # center for the clustering over the random reference set
            ref_disp[i] = kmeans.inertia_
        # Fit k clusters to the data
        kmeans_params["n_clusters"] = k
        kmeans = skl_KMeans(**kmeans_params)
        kmeans.fit(X)
        # Calculate gap statistic and its std using the SEM (standart deviation of the mean)
        gap = np.log(np.mean(ref_disp)) - np.log(kmeans.inertia_)
        gap_measurements.append(gap)
        std = np.std(ref_disp) / (kmeans.inertia_ * np.sqrt(n_refs))
        gap_std.append(
            max(
                abs(np.log(gap + std) - np.log(gap)),
                abs(np.log(gap - std) - np.log(gap)),
            )
        )

    gap_measurements = np.array(gap_measurements)
    gap_std = np.array(gap_std)
    cluster_numbers = np.arange(min_clusters, max_clusters + 1, step_clusters)
    criterion = "higher"

    """ Find the optimal number of topics using a visual inspection """
    plt.errorbar(x=cluster_numbers, y=gap_measurements, yerr=gap_std)
    plt.title(criterion)
    plt.xlabel("Number of topics")
    plt.ylabel("gap_statistics values")
    if show:
        plt.show(
            block=False
        )  # block=False should allow to continue the execution of the program while the graph is open
        while True:
            try:
                n_clusters = int(input("Input the optimal number of topics: "))
                if cluster_numbers[0] <= n_clusters <= cluster_numbers[-1]:
                    break
                else:
                    print(
                        "The input should be between {} and {}, try with another value.".format(
                            cluster_numbers[0], cluster_numbers[-1]
                        )
                    )
            except ValueError:
                print(
                    "The input value does not correspond to an integer, try with another value:"
                )
    else:
        plt.savefig("../outliers/gap_statistics_fine.pdf", dpi=300, bbox_inches="tight")
        n_clusters = "Look at the plot"
    plt.close()
    return n_clusters


def kmeans_clustering(vectors, num_clusters=None, verbose=False, kmeans_params={}):
    """
    Performs a kmeans clustering over the vectors

    Params
    ------
    vectors: numpy ndarray of shape (N,M)
        Vectors to cluster. N is the amount of vectors and M their dimension.
    num_clusters: int
        Number of clusters to fit.
    verbose: bool
        Whether to print information of the clustering or not.
    kmeans_parmas: dict
        Parameters of the clustering method

    Returns
    -------
    labels: numpy ndarrya of shape (N,)
        Labels of the cluster for each data point.
    """

    if isinstance(vectors, list):
        X = np.vstack(vectors)
    else:
        X = vectors
    if X.ndim != 2:
        raise ValueError(
            "The array containing the data does not have two (2) dimensions, it has {} dimensions".format(
                X.ndim
            )
        )

    if num_clusters is not None:
        n_clusters = num_clusters
    else:
        n_clusters = gap_statistics(
            vectors=puntajes,
            kmeans_params=kmeans_params,
            min_clusters=2,
            max_clusters=18,
            verbose=verbose,
            n_refs=2,
        )
    kmeans_params["n_clusters"] = n_clusters
    kmeans = skl_KMeans(**kmeans_params)
    kmeans.fit(X)

    return kmeans.labels_


def compute_outstanding_students(puntajes, punt):
    """
    Find the outstanding students inside each group (cluster)

    Params
    ------
    puntajes: numpy array of shape (N,M)
        Array of scores of the students. N is the number of students and M is the sum of:
        the amount of scores per student, 1 column that accounts for the group of the student
    punt: array of str
        List of scores to take into account.

    Returns
    -------
    puntajes: pandas array of shape (N,M)
        Array of scores of the students. N is the number of students and M is the sum of:
        the amount of scores per student, 1 column that accounts for the group of the
        student, 1 column that contains the difference between real and the predicted
        value, and 1 column that contains and indicator of whether the student is
        outstanding or not.
    """
    puntajes["diffs"] = np.empty(puntajes.shape[0])
    for id_ in tqdm(
        puntajes.cluster_id.unique(),
        total=len(puntajes.cluster_id.unique()),
        desc="Group: ",
    ):
        group = puntajes[puntajes.cluster_id == id_]
        for student_id in group.index:
            student_scores = group.loc[student_id, punt].to_numpy()
            group_scores = group[punt].drop(index=student_id).to_numpy()
            puntajes.loc[student_id, "diffs"] = is_outstanding(
                student_scores, group_scores
            )
    q = puntajes.diffs.sort_values().iloc[-num_genE]
    puntajes["which"] = puntajes.diffs >= q
    return puntajes


### Calculations

data_for_training = "../../../data/Data_For_Training/"
complete_data_digest_path = "../../../data/digests/icfes_20182_complete"
with open(complete_data_digest_path, "r") as f:
    digest = f.read()
    digest_in_disk = sha256sum(data_for_training + "icfes_20182_complete.csv")
    assert digest == digest_in_disk

data_complete = pd.read_csv(data_for_training + "icfes_20182_complete.csv")

punt = [
    col for col in data_complete.columns.values if "DECILE" in col and "pred" not in col
]
punt_pred = [
    col for col in data_complete.columns.values if "DECILE" in col and "pred" in col
]

puntajes = data_complete[punt + punt_pred]
print("Total number of students:", puntajes.shape[0])
puntajes.dropna(inplace=True)
print("After drop:", puntajes.shape[0])

del data_complete

# Perform the clustering

# n_clusters = gap_statistics(vectors=puntajes[punt_pred].to_numpy(), kmeans_params={}, min_clusters=50, max_clusters=200,verbose=True, # n_refs=30, step_clusters=10, show=False)
# print('Best number of clusters is:', n_clusters)
# From an analysis of gap statistics, we concluded that the best number of clusters is 120 https://gitlab.com/hubrain/ICFES-SocioEconomico/-/merge_requests/42
n_clusters = 120
print("starting clustering")
cluster_id = kmeans_clustering(
    vectors=puntajes[punt_pred].to_numpy(),
    verbose=True,
    num_clusters=n_clusters,
    kmeans_params={"n_init": 100, "max_iter": 10000, "n_jobs": -1},
)
puntajes.insert(0, "cluster_id", cluster_id, True)

puntajes = compute_outstanding_students(puntajes, punt)

# # We save the index because we want to check later if generación E match the outliers found here
puntajes.to_csv("../outliers/group_comparison_outliers.csv.gz", index=True, compression='gzip')
