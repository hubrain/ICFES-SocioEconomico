import pandas as pd
import numpy as np
import os
import json

# lista de las columnas que se van a convertir a one-hot con pd.get_dummies
columns_with_get_dummies = [
    "ESTU_DEPTO_RESIDE",
    "COLE_GENERO",
    "COLE_CALENDARIO",
    "COLE_CARACTER",
    "COLE_JORNADA",
    "COLE_DEPTO_UBICACION",
    "ESTU_DEPTO_PRESENTACION",
    "ESTU_TIPOREMUNERACION",
]
# lista de las columnas que se van a convertir en float
numeric_var = [
    "PUNT_LECTURA_CRITICA",
    "PERCENTIL_LECTURA_CRITICA",
    "DESEMP_LECTURA_CRITICA",
    "PUNT_MATEMATICAS",
    "PERCENTIL_MATEMATICAS",
    "DESEMP_MATEMATICAS",
    "PUNT_C_NATURALES",
    "PERCENTIL_C_NATURALES",
    "DESEMP_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "PERCENTIL_SOCIALES_CIUDADANAS",
    "DESEMP_SOCIALES_CIUDADANAS",
    "PUNT_INGLES",
    "PERCENTIL_INGLES",
    "PUNT_GLOBAL",
    "PERCENTIL_GLOBAL",
    "ESTU_INSE_INDIVIDUAL",
    "ESTU_NSE_ESTABLECIMIENTO",
]
# lista de la columnas a las que toca hacerles funciones de parsing
for_parsing = [
    "DESEMP_INGLES",
    "ESTU_NSE_INDIVIDUAL",
    "ESTU_NACIONALIDAD",
    "ESTU_GENERO",
    "ESTU_FECHANACIMIENTO",
    "ESTU_PAIS_RESIDE",
    "ESTU_ETNIA",
    "ESTU_LIMITA_MOTRIZ",
    "FAMI_ESTRATOVIVIENDA",
    "FAMI_PERSONASHOGAR",
    "FAMI_CUARTOSHOGAR",
    "FAMI_TIENEINTERNET",
    "FAMI_TIENESERVICIOTV",
    "FAMI_TIENECOMPUTADOR",
    "FAMI_TIENELAVADORA",
    "FAMI_TIENEHORNOMICROOGAS",
    "FAMI_TIENEAUTOMOVIL",
    "FAMI_TIENEMOTOCICLETA",
    "FAMI_TIENECONSOLAVIDEOJUEGOS",
    "FAMI_NUMLIBROS",
    "FAMI_COMELECHEDERIVADOS",
    "FAMI_COMECARNEPESCADOHUEVO",
    "FAMI_COMECEREALFRUTOSLEGUMBRE",
    "FAMI_SITUACIONECONOMICA",
    "ESTU_DEDICACIONLECTURADIARIA",
    "ESTU_DEDICACIONINTERNET",
    "ESTU_HORASSEMANATRABAJA",
    "COLE_NATURALEZA",
    "COLE_BILINGUE",
    "COLE_SEDE_PRINCIPAL",
    "COLE_AREA_UBICACION",
    "ESTU_PRIVADO_LIBERTAD",
]


# For parsing
def parse_desemp_ingles(val):
    if val == "A-":
        return 1
    elif val == "A1":
        return 2
    elif val == "A2":
        return 3
    elif val == "B+":
        return 4
    elif val == "B1":
        return 5
    else:
        return np.nan


def parse_NSE(val):
    if val == "NSE1":
        return 1
    elif val == "NSE2":
        return 2
    elif val == "NSE3":
        return 3
    elif val == "NSE4":
        return 4
    else:
        return np.nan


def parse_nacionalidad(value):
    if value == "Colombia":
        return 1
    elif value != np.NaN:
        return 0
    else:
        return np.NaN


def parse_genero(val):
    if val == "M":
        return 1
    elif val == "F":
        return 0
    else:
        return np.nan


def parse_nacimiento(val, year="01-01-2014", min_year=pd.to_datetime("1985-01-01")):
    date = max(pd.to_datetime(val, dayfirst=True, errors="coerce"), min_year)
    return (pd.to_datetime(year, dayfirst=True) - date).days


def parse_etnia(value):
    if value == "Ninguno":
        return 0
    elif value != np.NaN:
        return 1
    else:
        return np.NaN


def parse_indicador_x(val):
    if val == "x":
        return 1
    else:
        return 0


def parse_estrato(val):
    if val == "Estrato 1":
        return 1
    elif val == "Estrato 2":
        return 2
    elif val == "Estrato 3":
        return 3
    elif val == "Estrato 4":
        return 4
    elif val == "Estrato 5":
        return 5
    elif val == "Estrato 6":
        return 6
    else:
        return np.nan


def parse_numbers(val):
    if val == "Tres":
        return 3
    elif val == "Dos":
        return 2
    elif val == "Cuatro":
        return 4
    elif val == "Cinco":
        return 5
    elif val == "Uno":
        return 1
    elif val == "Seis":
        return 6
    elif val == "Siete":
        return 7
    elif val == "Ocho":
        return 8
    elif val == "Una":
        return 1
    elif val == "Diez":
        return 10
    elif val == "Once":
        return 11
    elif val == "Doce o más":
        return 12
    elif val == "Diez o más":
        return 10
    elif val == "Nueve":
        return 9
    elif val == "1 a 2":
        return 1.5
    elif val == "3 a 4":
        return 3.5
    elif val == "5 a 6":
        return 5.5
    elif val == "7 a 8":
        return 7.5
    elif val == "9 o más":
        return 9.5
    elif val == "Seis o mas":
        return 6.5
    elif val == "Ninguno":
        return 0
    else:
        return np.nan


def parse_Si_No(val):
    if val == "Si":
        return 1
    elif val == "No":
        return 0
    else:
        return np.nan


def parse_num_libros(val):
    if val == "0 A 10 LIBROS":
        return 0
    elif val == "11 A 25 LIBROS":
        return 1
    elif val == "26 A 100 LIBROS":
        return 2
    elif val == "MÁS DE 100 LIBROS":
        return 3
    else:
        return np.nan


def parse_fami_come(val):
    if val == "Nunca o rara vez comemos eso":
        return 0
    elif val == "1 o 2 veces por semana":
        return 1
    elif val == "3 a 5 veces por semana":
        return 2
    elif val == "Todos o casi todos los días":
        return 3
    else:
        return np.nan


def parse_fami_situa_econo(val):
    if val == "Peor":
        return 0
    elif val == "Igual":
        return 1
    elif val == "Mejor":
        return 2
    else:
        return np.nan


def parse_estu_lectura_diaria(val):
    if val == "No leo por entretenimiento":
        return 0
    elif val == "30 minutos o menos":
        return 1
    elif val == "Entre 30 y 60 minutos":
        return 2
    elif val == "Entre 1 y 2 horas":
        return 3
    elif val == "Más de 2 horas":
        return 4
    else:
        return np.nan


def parse_estu_dedica_internet(val):
    if val == "No Navega Internet":
        return 0
    elif val == "30 minutos o menos":
        return 1
    elif val == "Entre 30 y 60 minutos":
        return 2
    elif val == "Entre 1 y 3 horas":
        return 3
    elif val == "Más de 3 horas":
        return 4
    else:
        return np.nan


def parse_estu_horas_trabaja(val):
    if val == "0":
        return 0
    elif val == "Menos de 10 horas":
        return 1
    elif val == "Entre 11 y 20 horas":
        return 2
    elif val == "Entre 21 y 30 horas":
        return 3
    elif val == "Más de 30 horas":
        return 4
    else:
        return np.nan


def parse_cole_naturaleza(val):
    if val == "NO OFICIAL":
        return 0
    elif val == "OFICIAL":
        return 1
    else:
        return np.nan


def parse_S_N(val):
    if val == "S":
        return 1
    elif val == "N":
        return 0
    else:
        return np.nan


def parse_cole_area_ubic(val):
    if val == "URBANO":
        return 1
    elif val == "RURAL":
        return 0
    else:
        return np.nan


# For parsing 'FAMI_EDUCACIONMADRE' y 'FAMI_EDUCACIONPADRE'
def parse_nivel_educacion(val):
    if val == "No sabe" or val == "No aplica" or val == "No Aplica":
        return [0, 1]
    else:
        if val == "Ninguno":
            return [0, 0]
        elif val == "Primaria incompleta":
            return [1, 0]
        elif val == "Primaria completa":
            return [2, 0]
        elif val == "Secundaria (Bachillerato) incompleta":
            return [3, 0]
        elif val == "Secundaria (Bachillerato) completa":
            return [4, 0]
        elif val == "Técnica o tecnológica incompleta":
            return [5, 0]
        elif val == "Técnica o tecnológica completa":
            return [6, 0]
        elif val == "Educación profesional incompleta":
            return [7, 0]
        elif val == "Educación profesional completa":
            return [8, 0]
        elif val == "Postgrado":
            return [9, 0]
        else:
            return [np.nan, np.nan]


# Jerarquia de ocupaciones diseñada por Oscar y Brayan
def parse_trabajo_labor_padres(val):
    """
    Parsing the type of ocupation of the parents to a ranking based on skill level,
    social status and the Clasificación Internacional Uniforme de Ocupaciones.
    Adaptación Colombia (CIUO A.C.)
    ______________________________________________________________________________________
    Parameters:
        val: str containing the answer to the question
    Returns:
        Lista: List with four items
            Lista[0] ranking given by Oscar and Brayan
            Lista[1] Clasificación Internacional Uniforme de Ocupaciones. Adaptación Colombia (CIUO A.C.)
            Lista[2] Skill level
            Lista[3] Social status ranking inside each skill level (Bajo = 1, Medio = 2, Medio-Alto = 3, Alto = 4)
    """
    if val == "No sabe":
        return [0, np.nan, 0, np.nan]
    elif val == "No aplica":
        return [0, np.nan, 0, np.nan]
    elif val == "Pensionado":
        return [1, np.nan, 1, 1]
    elif val == "Trabaja en el hogar, no trabaja o estudia":
        return [2, 1, 1, 1]
    elif (
        val
        == "Trabaja como personal de limpieza, mantenimiento, seguridad o construcción"
    ):
        return [3, 1, 1, 1]
    elif val == "Trabaja por cuenta propia (por ejemplo plomero, electricista)":
        return [4, 3, 2, 1]
    elif val == "Es operario de máquinas o conduce vehículos (taxita, chofer)":
        return [5, 3, 2, 1]
    elif val == "Es agricultor, pesquero o jornalero":
        return [6, 4, 2, 1]
    elif val == "Es vendedor o trabaja en atención al público":
        return [7, 5, 2, 2]
    elif (
        val
        == "Es dueño de un negocio pequeño (tiene pocos empleados o no tiene, por ejemplo tienda, papelería, etc"
    ):
        return [8, 5, 2, 2]
    elif (
        val
        == "Tiene un trabajo de tipo auxiliar administrativo (por ejemplo, secretario o asistente)"
    ):
        return [9, 6, 2, 2]
    elif val == "Trabaja como profesional (por ejemplo médico, abogado, ingeniero)":
        return [10, 8, 4, 4]
    elif (
        val
        == "Es dueño de un negocio grande, tiene un cargo de nivel directivo o gerencial"
    ):
        return [11, 9, 4, 4]
    else:
        return [np.nan, np.nan, np.nan, np.nan]


# For adding information of a Municipio
def parse_mun_X(codigo, dictX):
    if codigo in dictX.keys():
        return dictX[codigo]
    else:
        return np.nan


#### Calcular la nota Z
def Calculate_Nota_Z(category, puntajes, df):
    usefull_cols = puntajes.copy()
    usefull_cols.append(category)
    df_notas = df[usefull_cols]
    name = "NOTA_Z_" + category + "_"

    # Create columns to store Z scores and fill them initially with nans
    for puntaje in puntajes:
        df_notas.loc[:, name + puntaje] = np.nan

    colegios_counter = 0
    for groupname, group in df_notas.groupby(category):
        # Check if group has more than 20 students:
        if group.shape[0] < 20:
            continue
        else:
            colegios_counter += 1

        for puntaje in puntajes:
            mu = group[puntaje].mean()
            std = group[puntaje].std()
            if std != 0:
                df.loc[group.index, name + puntaje] = (
                    group[puntaje].to_numpy() - mu
                ) / std
            else:
                continue

    print("Colegios contados con mínimo 20 estudiantes:", colegios_counter)
    nota_z_names = [name + puntaje for puntaje in puntajes]
    return df, nota_z_names


def calculate_decile(puntaje, df):
    np.random.seed(0)
    maxval = df[puntaje].max()
    minval = df[puntaje].min()
    size = df[puntaje].shape[0]
    rangeval = (maxval - minval) / 10000
    df.loc[:, "DECILE_" + puntaje] = pd.qcut(
        df[puntaje] + np.random.uniform(0, rangeval, size), 1000, labels=False
    )
    return df


def compute_all_deciles(puntajes, df):
    col_names = []
    for puntaje in puntajes:
        df = calculate_decile(puntaje, df)
        col_names.append("DECILE_" + puntaje)
    return df, col_names


# For filling


def fill_etnia(val):
    if pd.isnull(val):
        return "Ninguno"
    else:
        return val


# Diccionario de las columnas que tienen asociada una función para hacer parsing
column_2_func = {}

column_2_func["DESEMP_INGLES"] = parse_desemp_ingles
column_2_func["ESTU_NSE_INDIVIDUAL"] = parse_NSE
column_2_func["ESTU_NACIONALIDAD"] = parse_nacionalidad
column_2_func["ESTU_GENERO"] = parse_genero
column_2_func["ESTU_FECHANACIMIENTO"] = parse_nacimiento
column_2_func["ESTU_PAIS_RESIDE"] = parse_nacionalidad
column_2_func["ESTU_ETNIA"] = parse_etnia
column_2_func["ESTU_LIMITA_MOTRIZ"] = parse_indicador_x
column_2_func["FAMI_ESTRATOVIVIENDA"] = parse_estrato
column_2_func["FAMI_PERSONASHOGAR"] = parse_numbers
column_2_func["FAMI_CUARTOSHOGAR"] = parse_numbers
column_2_func["FAMI_TIENEINTERNET"] = parse_Si_No
column_2_func["FAMI_TIENESERVICIOTV"] = parse_Si_No
column_2_func["FAMI_TIENECOMPUTADOR"] = parse_Si_No
column_2_func["FAMI_TIENELAVADORA"] = parse_Si_No
column_2_func["FAMI_TIENEHORNOMICROOGAS"] = parse_Si_No
column_2_func["FAMI_TIENEAUTOMOVIL"] = parse_Si_No
column_2_func["FAMI_TIENEMOTOCICLETA"] = parse_Si_No
column_2_func["FAMI_TIENECONSOLAVIDEOJUEGOS"] = parse_Si_No
column_2_func["FAMI_NUMLIBROS"] = parse_num_libros
column_2_func["FAMI_COMELECHEDERIVADOS"] = parse_fami_come
column_2_func["FAMI_COMECARNEPESCADOHUEVO"] = parse_fami_come
column_2_func["FAMI_COMECEREALFRUTOSLEGUMBRE"] = parse_fami_come
column_2_func["FAMI_SITUACIONECONOMICA"] = parse_fami_situa_econo
column_2_func["ESTU_DEDICACIONLECTURADIARIA"] = parse_estu_lectura_diaria
column_2_func["ESTU_DEDICACIONINTERNET"] = parse_estu_dedica_internet
column_2_func["ESTU_HORASSEMANATRABAJA"] = parse_estu_horas_trabaja
column_2_func["COLE_NATURALEZA"] = parse_cole_naturaleza
column_2_func["COLE_BILINGUE"] = parse_S_N
column_2_func["COLE_SEDE_PRINCIPAL"] = parse_S_N
column_2_func["COLE_AREA_UBICACION"] = parse_cole_area_ubic
column_2_func["ESTU_PRIVADO_LIBERTAD"] = parse_S_N
column_2_func["FAMI_EDUCACIONPADRE"] = parse_nivel_educacion
column_2_func["FAMI_EDUCACIONMADRE"] = parse_nivel_educacion
column_2_func["FAMI_TRABAJOLABORPADRE"] = parse_trabajo_labor_padres
column_2_func["FAMI_TRABAJOLABORMADRE"] = parse_trabajo_labor_padres

# Diccionario de columnas que tiene asociada una función para llenar valores faltantes o mal parseados
fill_values = {}

fill_values["ESTU_ETNIA"] = fill_etnia

# Loading data
data_dir = "../../../data/unziped_data/"
df = pd.read_csv(
    os.path.join(data_dir, "FTP_SABER11_20182.TXT"),
    sep="¬",
    header=0,
    encoding="utf-8-sig",
)


# Agregar la nota z por colegio y los mililes
puntajes = [
    "PUNT_MATEMATICAS",
    "PUNT_LECTURA_CRITICA",
    "PUNT_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "PUNT_INGLES",
    "PUNT_GLOBAL",
]

df, names_nota_z = Calculate_Nota_Z("COLE_CODIGO_ICFES", puntajes, df)
df, names_deciles = compute_all_deciles(puntajes, df)
numeric_var = numeric_var + names_nota_z + names_deciles

codigos = [
    "ESTU_COD_RESIDE_MCPIO",
    "COLE_COD_MCPIO_UBICACION",
    "ESTU_COD_MCPIO_PRESENTACION",
]

to_keep = list(column_2_func.keys()) + columns_with_get_dummies + numeric_var
df_vars = df[to_keep]

for col, func in fill_values.items():
    df_vars[col] = df_vars[col].apply(lambda x: func(x))

df_vars = pd.get_dummies(df_vars, columns=columns_with_get_dummies, drop_first=True)

for col, func in column_2_func.items():
    df_vars[col] = df_vars[col].apply(lambda x: func(x))

# Some columns are parsed to a list of values, so the values inside those lists are parsed to different columns
## Educacion de los padres
df_vars[
    ["FAMI_EDUCACIONMADRE_Nivel_Educativo", "FAMI_EDUCACIONMADRE_No_Aplica_No_Sabe"]
] = pd.DataFrame(df_vars.FAMI_EDUCACIONMADRE.tolist(), index=df_vars.index)
df_vars[
    ["FAMI_EDUCACIONPADRE_Nivel_Educativo", "FAMI_EDUCACIONPADRE_No_Aplica_No_Sabe"]
] = pd.DataFrame(df_vars.FAMI_EDUCACIONPADRE.tolist(), index=df_vars.index)
df_vars = df_vars.drop(["FAMI_EDUCACIONPADRE", "FAMI_EDUCACIONMADRE"], axis=1)
## Trabajo o labor de los padres
df_vars[
    [
        "FAMI_TRABAJOLABORPADRE_ranking",
        "FAMI_TRABAJOLABORPADRE_CIUO",
        "FAMI_TRABAJOLABORPADRE_skill_level",
        "FAMI_TRABAJOLABORPADRE_social_status",
    ]
] = pd.DataFrame(df_vars.FAMI_TRABAJOLABORPADRE.tolist(), index=df_vars.index)
df_vars[
    [
        "FAMI_TRABAJOLABORMADRE_ranking",
        "FAMI_TRABAJOLABORMADRE_CIUO",
        "FAMI_TRABAJOLABORMADRE_skill_level",
        "FAMI_TRABAJOLABORMADRE_social_status",
    ]
] = pd.DataFrame(df_vars.FAMI_TRABAJOLABORMADRE.tolist(), index=df_vars.index)
df_vars = df_vars.drop(["FAMI_TRABAJOLABORPADRE", "FAMI_TRABAJOLABORMADRE"], axis=1)

# Include information contained in the dictionaries about the towns where the children live
file_mun1 = "dic_mun_gradoimportancia.json"
file_mun2 = "dic_mun_pesorelativo.json"
file_mun3 = "dic_mun_poblacion.json"
file_mun4 = "dic_mun_valoragregado.json"

files_mun = [file_mun1, file_mun2, file_mun3, file_mun4]


def extract_dict(json_name, dicc_directory):
    with open(dicc_directory + json_name, "r") as file:
        json_dict = json.load(file)
    return json_dict


def num2str(num):
    try:
        return str(int(num))
    except:
        return str(num)


for col in [cod for cod in codigos if "MCPIO" in cod]:
    for file in files_mun:
        dictX = extract_dict(file, "../../../data/Diccionarios_mun/")
        df_vars[col + file[:-5]] = df[col].apply(
            lambda x: parse_mun_X(num2str(x), dictX)
        )

data_for_training = "../../../data/Data_For_Training/"

df_vars.to_csv(data_for_training + "icfes_20182.csv", index=False)


for name in names_nota_z + names_deciles:
    print(name, df_vars[name].isna().sum() / df_vars[name].shape[0])
