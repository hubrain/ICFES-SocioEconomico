import pandas as pd
import numpy as np
import sys
sys.path.append("../..")
from utils import sha256sum

# Generación E number of elegible people are in https://gitlab.com/hubrain/ICFES-SocioEconomico/-/merge_requests/49
num_genE = 3737 + 100

def metric_student(df,variables_list=['DECILE_PUNT_MATEMATICAS','DECILE_PUNT_LECTURA_CRITICA','DECILE_PUNT_C_NATURALES','DECILE_PUNT_SOCIALES_CIUDADANAS',
        'DECILE_PUNT_INGLES','DECILE_PUNT_GLOBAL'],metric = [1,1,1,1,1,1]):
    '''
    Parameters
    ----------
    df: pandas.DataFrame
        Data Frame complete `icfes_20182_complete.csv`

    variables_list: list
        List of puntajes to have account `DECILE_PUNT_...`
    
    '''
    assert len(variables_list) == len(metric)
    list_ss = []
    for var in variables_list:
        print(var)
        list_ss.append(np.array(df[var]-df[var+"_pred"]))
    
    list_ss = np.array(list_ss)
    vector_student = np.transpose(list_ss) * np.array(metric)
    difference = np.mean(vector_student, axis=1)
    quantile = np.sort(difference)[-num_genE]
    print(quantile)

    which = (difference >= quantile)

    return difference, which


data_for_training = "../../../data/Data_For_Training/"
complete_data_digest_path = "../../../data/digests/icfes_20182_complete"
with open(complete_data_digest_path, "r") as f:
    digest = f.read()
    digest_in_disk = sha256sum(data_for_training + "icfes_20182_complete.csv")
    assert digest == digest_in_disk

data_complete = pd.read_csv(data_for_training + "icfes_20182_complete.csv")

variables = [col for col in data_complete.columns.values if "DECILE" in col and "pred" not in col]
variables_pred = [col for col in data_complete.columns.values if "DECILE" in col and "pred" in col]

difference, which = metric_student(data_complete,variables_list=variables)

puntajes = data_complete[variables + variables_pred]
puntajes["diffs"] = difference
puntajes['which'] = which

# We save the index because we want to check later if generación E match the outliers found here
puntajes.to_csv("../outliers/direct_comparison_outliers.csv.gz", index=True, compression='gzip')