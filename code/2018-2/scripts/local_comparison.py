"""
Run create_complete_data before running this code
"""

import pandas as pd
import numpy as np
from tqdm import tqdm
import sys
sys.path.append("../..")
from utils import sha256sum

# Generación E number of elegible people are in https://gitlab.com/hubrain/ICFES-SocioEconomico/-/merge_requests/49
num_genE = 3737 + 100

def closest_k(id_, puntajes, predcols, k=100):
    student = puntajes.loc[id_]
    puntajes = puntajes.drop(index=id_)
    puntajes_pred = puntajes[predcols].to_numpy()
    student_pred = student[predcols].to_numpy()
    dists = np.sum(np.abs(puntajes_pred - student_pred)**2, axis=1)
    idx = np.argsort(dists)[:k]
    return puntajes.index[idx]

def is_outstanding(student, group):
    """
    The function computes how outstanding a student is

    Parameters
    ----------
    student: numpy.array of shape (N,)
        The true scores of the student, there are N of them
    group: numpy.array of shape(k, N)
        The true scores of the students corresponding to the
        group of closest
    """
    μ = np.mean(group, axis=0)
    return np.sum(student - μ)

if __name__ == "__main__":
    
    data_for_training = "../../../data/Data_For_Training/"
    complete_data_digest_path = "../../../data/digests/icfes_20182_complete"
    with open(complete_data_digest_path, "r") as f:
        digest = f.read()
        digest_in_disk = sha256sum(data_for_training + "icfes_20182_complete.csv")
        assert digest == digest_in_disk

    data_complete = pd.read_csv(data_for_training + "icfes_20182_complete.csv")

    punt = [col for col in data_complete.columns.values if "DECILE" in col and "pred" not in col]
    punt_pred = [col for col in data_complete.columns.values if "DECILE" in col and "pred" in col]

    puntajes = data_complete[punt + punt_pred]
    print('Total number of students:', puntajes.shape[0])
    puntajes.dropna(inplace=True)
    print('After drop:', puntajes.shape[0])

    del data_complete

    diffs = np.empty(puntajes.shape[0])
    puntajes["diffs"] = diffs

    for id_ in tqdm(puntajes.index, total=len(puntajes.index)):
        student = puntajes.loc[id_, punt].to_numpy()
        idx = closest_k(id_, puntajes, punt_pred, k=100)
        group = puntajes.loc[idx, punt].to_numpy()
        puntajes.loc[id_, "diffs"] = is_outstanding(student, group)

    # Pick the best
    q = puntajes.diffs.sort_values().iloc[-num_genE]
    puntajes["which"] = puntajes.diffs >= q

    # We save the index because we want to check later if generación E match the outliers found here
    puntajes.to_csv("../outliers/local_comparison_outliers.csv.gz", index=True, compression='gzip')
