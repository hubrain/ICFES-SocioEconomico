import pandas as pd
import numpy as np
import os
from sklearn.preprocessing import StandardScaler
import xgboost as xgb
import pickle
import sys
sys.path.append("../..")
from utils import sha256sum

# Data directory
data_dir = "../../../data/unziped_data/"
data_original = pd.read_csv(
    os.path.join(data_dir, "FTP_SABER11_20182.TXT"),
    sep="¬",
    header=0,
    encoding="utf-8-sig",
)
data_original = data_original.add_suffix("_original")

# Open data with pandas
data_for_training = "../../../data/Data_For_Training/"
data_processed = pd.read_csv(data_for_training + "icfes_20182.csv")

# Fill with the predicted values

# lista de las columnas con las caracteristicas a predecir
notasz = [c for c in data_processed.columns.values if c.startswith("NOTA_Z")]
deciles = [c for c in data_processed.columns.values if c.startswith("DECILE")]
labels_names = notasz + deciles

# lista de las columnas que se van a omitir de las caracteristicas
vars_2_omit = (
    [
        "PUNT_LECTURA_CRITICA",
        "PERCENTIL_LECTURA_CRITICA",
        "DESEMP_LECTURA_CRITICA",
        "PUNT_MATEMATICAS",
        "PERCENTIL_MATEMATICAS",
        "DESEMP_MATEMATICAS",
        "PUNT_C_NATURALES",
        "PERCENTIL_C_NATURALES",
        "DESEMP_C_NATURALES",
        "PUNT_SOCIALES_CIUDADANAS",
        "PERCENTIL_SOCIALES_CIUDADANAS",
        "DESEMP_SOCIALES_CIUDADANAS",
        "PUNT_INGLES",
        "PERCENTIL_INGLES",
        "DESEMP_INGLES",
        "PUNT_GLOBAL",
        "PERCENTIL_GLOBAL",
    ]
    + deciles
    + notasz
)

labels = data_processed[labels_names]
features = data_processed.drop(vars_2_omit, axis=1)
# Mascara para los modelos que predicen la Nota Z
mask_nota_z = [
    False
    if "COLE" in col or "DEPTO" in col or "MCPIO" in col or "ESTABLECIMIENTO" in col
    else True
    for col in features.columns
]

# Cargar los escaladores
scaler_features = pickle.load(open("../models/scaler_features_20182.pkl", "rb"))
scaler_labels = pickle.load(open("../models/scaler_labels_20182.pkl", "rb"))

# Escalar las caracteristicas
X = scaler_features.transform(features)
Y = scaler_labels.transform(labels)

# Hacer las predicciones de los resultados
Y_pred = np.zeros(labels.shape)
Y_pred.fill(np.nan)
for ii in range(len(labels_names)):
    print(labels_names[ii])
    bst = xgb.Booster({"nthread": 4})  # init model
    bst.load_model("../models/icfes_20182_{0}.model".format(labels_names[ii]))
    idx = np.where(~np.isnan(Y[:, ii]))
    if "NOTA_Z" in labels_names[ii]:
        dtotal = xgb.DMatrix(X[idx[0]][:, mask_nota_z])
    else:
        dtotal = xgb.DMatrix(X[idx[0]])
    ypred = bst.predict(dtotal)
    Y_pred[idx[0], ii] = ypred

# Crear dataframe con labels no escaladas y prediciones no escaladas
Y_pred_real = scaler_labels.inverse_transform(Y_pred)
pred_lables = [label + "_pred" for label in labels_names]
columns = (
    data_original.columns.to_list()
    + features.columns.to_list()
    + labels_names
    + pred_lables
)
total_data = np.concatenate(
    (
        data_original.values,
        features.values,
        labels.values,
        Y_pred_real,
    ),
    axis=1,
)
data_complete = pd.DataFrame(data=total_data, columns=columns)
print(data_complete.columns.to_list())
complete_name = "icfes_20182_complete.csv"
data_complete.to_csv(data_for_training + complete_name, index=True)

# Write SHA256
digests_path = "../../../data/digests"
digest = sha256sum(data_for_training + complete_name)

with open(os.path.join(digests_path, complete_name.split('.')[0]), "w") as f:
    f.write(digest)