import pandas as pd
from matplotlib import pyplot as plt
import os

data_for_training = "../../../data/Data_For_Training/"
data_complete = pd.read_csv(data_for_training + "icfes_20182_complete.csv")
genE = data_complete["ESTU_GENERACION-E_original"]
sobresaliente_name = 'GenE'
data_complete["sobresaliente"] = genE.apply(lambda g: sobresaliente_name if g == "GENERACION E - EXCELENCIA NACIONAL" or g == "GENERACION E - EXCELENCIA DEPARTAMENTAL" else "Resto")
resto = data_complete[data_complete.sobresaliente == 'Resto']
sobresalientes = data_complete[data_complete.sobresaliente == sobresaliente_name]
columns_with_get_dummies = [
    "ESTU_DEPTO_RESIDE",
    "COLE_GENERO",
    "COLE_CALENDARIO",
    "COLE_CARACTER",
    "COLE_JORNADA",
    "COLE_DEPTO_UBICACION",
    "ESTU_DEPTO_PRESENTACION",
    "ESTU_TIPOREMUNERACION",
]
# lista de las columnas que se van a convertir en float
numeric_var = [
    "PUNT_LECTURA_CRITICA",
    "PERCENTIL_LECTURA_CRITICA",
    "DESEMP_LECTURA_CRITICA",
    "PUNT_MATEMATICAS",
    "PERCENTIL_MATEMATICAS",
    "DESEMP_MATEMATICAS",
    "PUNT_C_NATURALES",
    "PERCENTIL_C_NATURALES",
    "DESEMP_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "PERCENTIL_SOCIALES_CIUDADANAS",
    "DESEMP_SOCIALES_CIUDADANAS",
    "PUNT_INGLES",
    "PERCENTIL_INGLES",
    "PUNT_GLOBAL",
    "PERCENTIL_GLOBAL",
    "ESTU_INSE_INDIVIDUAL",
    "ESTU_NSE_ESTABLECIMIENTO",
]
# lista de la columnas a las que toca hacerles funciones de parsing
for_parsing = [
    "DESEMP_INGLES",
    "ESTU_NSE_INDIVIDUAL",
    "ESTU_NACIONALIDAD",
    "ESTU_GENERO",
    "ESTU_PAIS_RESIDE",
    "ESTU_ETNIA",
    "ESTU_LIMITA_MOTRIZ",
    "FAMI_ESTRATOVIVIENDA",
    "FAMI_PERSONASHOGAR",
    "FAMI_CUARTOSHOGAR",
    "FAMI_TIENEINTERNET",
    "FAMI_TIENESERVICIOTV",
    "FAMI_TIENECOMPUTADOR",
    "FAMI_TIENELAVADORA",
    "FAMI_TIENEHORNOMICROOGAS",
    "FAMI_TIENEAUTOMOVIL",
    "FAMI_TIENEMOTOCICLETA",
    "FAMI_TIENECONSOLAVIDEOJUEGOS",
    "FAMI_NUMLIBROS",
    "FAMI_COMELECHEDERIVADOS",
    "FAMI_COMECARNEPESCADOHUEVO",
    "FAMI_COMECEREALFRUTOSLEGUMBRE",
    "FAMI_SITUACIONECONOMICA",
    "ESTU_DEDICACIONLECTURADIARIA",
    "ESTU_DEDICACIONINTERNET",
    "ESTU_HORASSEMANATRABAJA",
    "COLE_NATURALEZA",
    "COLE_BILINGUE",
    "COLE_SEDE_PRINCIPAL",
    "COLE_AREA_UBICACION",
    "ESTU_PRIVADO_LIBERTAD",
]

path = '../outliers/feature_histograms_raw_genE'
try:
    os.mkdir(path)
except:
    print('Path to save figures already exists') 
columns = columns_with_get_dummies + for_parsing 
columns_hist = [col+"_original" for col in columns]



for column in columns_hist:
    lenvals = data_complete[column].unique().size
    fig, axs = plt.subplots(1,2, figsize=(12,lenvals*1.5))
    labels_sobre = sobresalientes[column].value_counts(normalize=True).sort_index().index.values
    counts_sobre = sobresalientes[column].value_counts(normalize=True).sort_index().values * 100
    labels_resto = resto[column].value_counts(normalize=True).sort_index().index.values
    counts_resto = resto[column].value_counts(normalize=True).sort_index().values * 100
                                                                                  
    axs[0].barh(labels_sobre,counts_sobre,color='red',alpha=0.5,label='Generación E')
    axs[0].barh(labels_resto,counts_resto,color='blue',alpha=0.5,label='Resto')
    axs[0].legend(loc="upper right")
    
    labels_difer = (resto[column].value_counts(normalize=True)-sobresalientes[column].value_counts(normalize=True)).sort_index().index.values
    counts_difer = (resto[column].value_counts(normalize=True)-sobresalientes[column].value_counts(normalize=True)).sort_index().values * 100
    axs[0].tick_params(axis='x', rotation=90)
    axs[1].barh(labels_difer,counts_difer,color='green',label='diferencia (%)')
    axs[1].tick_params(axis='x', rotation=90)
    axs[1].legend(loc="upper right")
    plt.savefig(path+'/distribution_{0}.pdf'.format(column), bbox_inches='tight', dpi=300)
    plt.close()