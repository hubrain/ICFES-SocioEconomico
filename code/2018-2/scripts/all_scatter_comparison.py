import pandas as pd
import sys

sys.path.append("../../")
from utils import scatter

# Generación E number of elegible people are in https://gitlab.com/hubrain/ICFES-SocioEconomico/-/merge_requests/49
num_genE = 3737 + 100

def create_scatters(
    methods,
    y_label="Predicted global score",
    x_label="True global score",
    path_sys="../outliers/",
):

    for method in methods:
        df = pd.read_csv(path_sys + "{0}_comparison_outliers.csv.gz".format(method), compression='gzip')
        true = df.DECILE_PUNT_GLOBAL.values
        pred = df.DECILE_PUNT_GLOBAL_pred.values
        which = df.which
        scatter(
            true,
            pred,
            which,
            y_label=y_label,
            x_label=x_label,
            path=path_sys + f"{method}_scatter.png",
        )

def create_genE_scatters(
    y_label="Predicted global score",
    x_label="True global score",
    path_sys="../outliers/",
):
    data_for_training = "../../../data/Data_For_Training/"
    df = pd.read_csv(data_for_training + "icfes_20182_complete.csv", columns=["ESTU_GENERACION-E_original", "DECILE_PUNT_GLOBAL", "DECILE_PUNT_GLOBAL_pred"])
    genE = df["ESTU_GENERACION-E_original"]
    true = df.DECILE_PUNT_GLOBAL.values
    pred = df.DECILE_PUNT_GLOBAL_pred.values
    which = (genE == "GENERACION E - EXCELENCIA NACIONAL") | (genE == "GENERACION E - EXCELENCIA DEPARTAMENTAL")
    scatter(
            true,
            pred,
            which,
            y_label=y_label,
            x_label=x_label,
            path=path_sys + "genE_scatter.png",
        )

if __name__ == "__main__":
    path_sys = "../outliers/"
    create_scatters(["direct", "group", "local"], path_sys=path_sys)

    # Plot scatterplot for outstanding students with genE tags.
    data_for_training = "../../../data/Data_For_Training/"
    complete_name = "icfes_20182_complete.csv"
    cols = ['DECILE_PUNT_GLOBAL', 'DECILE_PUNT_GLOBAL_pred', 'ESTU_GENERACION-E_original']
    df = pd.read_csv(data_for_training + complete_name, usecols=cols)
    genE = df["ESTU_GENERACION-E_original"]
    df["which"] = genE.apply(lambda g: 1 if g == "GENERACION E - EXCELENCIA NACIONAL" or g == "GENERACION E - EXCELENCIA DEPARTAMENTAL" else 0)
    scatter(
        df.DECILE_PUNT_GLOBAL.to_numpy(),
        df.DECILE_PUNT_GLOBAL_pred.to_numpy(),
        df.which.to_numpy(),
        y_label="Predicted global score",
        x_label="True global score",
        path="../outliers/genE_scatter.png",
    )