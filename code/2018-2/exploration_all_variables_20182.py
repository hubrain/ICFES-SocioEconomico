import pandas as pd
import matplotlib.pyplot as plt
import os
from pandas_profiling import ProfileReport

data_dir = "../../data/unziped_data/"
df = pd.read_csv(os.path.join(data_dir, "FTP_SABER11_20182.TXT"), sep='¬', header=0, encoding='utf-8-sig')

profile = ProfileReport(df, title="Pandas Profiling Report", minimal=True)

profile.to_file("exploration_20182_report.html")