import pandas as pd
import ppscore as pps
import matplotlib.pyplot as plt
import seaborn as sns
from dython.nominal import cluster_correlations
import os

currpath = os.path.dirname(os.path.abspath(__file__))

df = pd.read_csv(
    os.path.join(currpath, "../../../data/unziped_data/FTP_SABER11_20141.TXT"),
    delimiter="¬",
)
interesting = [
    "ESTU_GENERO",
    "ESTU_VALORPENSIONCOLEGIO",
    "FAMI_EDUCACIONPADRE",
    "FAMI_EDUCACIONMADRE",
    "FAMI_OCUPACIONPADRE",
    "FAMI_OCUPACIONMADRE",
    "FAMI_ESTRATOVIVIENDA",
    "FAMI_NIVELSISBEN",
    "FAMI_PERSONASHOGAR",
    "FAMI_CUARTOSHOGAR",
    "FAMI_PISOSHOGAR",
    "FAMI_TIENEINTERNET",
    "FAMI_TIENECOMPUTADOR",
    "FAMI_TIENELAVADORA",
    "FAMI_TIENEMICROONDAS",
    "FAMI_TIENEHORNO",
    "FAMI_TIENETELEVISOR",
    "FAMI_TIENEAUTOMOVIL",
    "FAMI_TIENEDVD",
    "FAMI_TIENE_NEVERA",
    "FAMI_TIENE_CELULAR",
    "FAMI_INGRESOFMILIARMENSUAL",
    "ESTU_TRABAJAACTUALMENTE",
    "ESTU_ANOSPREESCOLAR",
    "ESTU_ANOSCOLEGIOACTUAL",
    "ESTU_TOTALALUMNOSCURSO",
    "ESTU_PUNT_ESPERADO_LENGUAJE",
    "ESTU_PUNT_ESPERADO_MATEMATICAS",
    "ESTU_PUNT_ESPERADO_INGLES",
    "COLE_GENERO",
    "COLE_NATURALEZA",
    "PUNT_MATEMATICAS",
    "PUNT_CIENCIAS_SOCIALES",
    "PUNT_INGLES",
    "PUNT_BIOLOGIA",
    "PUNT_FILOSOFIA",
    "PUNT_INTERDISC_MEDIOAMBIENTE",
    "PUNT_FISICA",
    "PUNT_QUIMICA",
    "PUNT_LENGUAJE",
    "PUNT_INTERDISC_VIOLENCIAYSOC",
    "ESTU_PUESTO",
]

print(f"There are {len(interesting)} interesting hand-selected variables.")

df_matrix = pps.matrix(df[interesting])
pps = df_matrix.round(2)

keep_cols = []
threshold = 0.32
for column in pps.columns:
    fill_row = ((pps.loc[column, :] >= threshold) & ((pps.loc[column, :] != 1))).any()
    fill_column = (
        (pps.loc[:, column] >= threshold) & ((pps.loc[:, column] != 1))
    ).any()
    if fill_row == True and fill_column == fill_row:
        keep_cols.append(column)

print(f"There are {len(keep_cols)} important columns.")

correlations, _ = cluster_correlations(pps.loc[keep_cols, keep_cols])

fig, ax = plt.subplots(figsize=(20, 15))
sns.heatmap(
    correlations, vmin=0, vmax=1, cmap="Blues", linewidths=0.5, annot=True, ax=ax
)
plt.savefig(
    os.path.join(currpath, "../../../figures/20141/predictive_score_matrix.pdf"),
    bbox_inches="tight",
    dpi=300,
)
