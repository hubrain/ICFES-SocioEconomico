import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import pickle
import umap
import traceback
import sys
import shutil

def load_model(f_name):
    try:  
        loaded_model = pickle.load((open(f_name, 'rb')))
        print("La carga de modelos de UMAP funciona bien")
    except:
        _ , _ , tb = sys.exc_info()
        errores = traceback.extract_tb(tb)
        file_dir = traceback.format_list([errores[3]])[0].split('"')[1][:-11]
        file_ = traceback.format_list([errores[3]])[0].split('"')[1]
        if("rp_trees.py" in file_):
            shutil.copy(file_,"./rp_trees_original.py")
            shutil.copy("./rp_trees_functional.py",file_dir+"rp_trees.py")
            print("The file rp_trees.py has been modified")

        else:
            raise Exception("The error is not the same as the one originally reported, do a debbuging of your error")
    
dir_models = "../models/"    
f_name = dir_models + 'sample_umap_model.sav'
load_model(f_name)
