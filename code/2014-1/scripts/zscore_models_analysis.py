import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
import xgboost as xgb
import pickle

currpath = os.path.dirname(os.path.abspath(__file__))
data_dir = os.path.join(currpath, "../../../data/unziped_data/")

df_FTP_SABER11_20141 = pd.read_csv(
    os.path.join(data_dir, "FTP_SABER11_20141.TXT"), delimiter="¬", engine="python"
)

# Open data with pandas

data_for_training = os.path.join(currpath, "../../../data/Data_For_Training/")
data = pd.read_csv(os.path.join(data_for_training, "icfes_20141.csv"))

notasz = [c for c in data.columns.values if c.startswith("NOTA_Z")]
deciles = [c for c in data.columns.values if c.startswith("DECILE")]
puntajes = [c for c in data.columns.values if c.startswith("PUNT")]

# lista de las columnas con las caracteristicas a predecir
labels_names = notasz + deciles

# lista de las columnas que se van a omitir de las caracteristicas
vars_2_omit = (
    [
        "PUNT_MATEMATICAS",
        "PUNT_CIENCIAS_SOCIALES",
        "PUNT_INGLES",
        "PUNT_BIOLOGIA",
        "PUNT_FILOSOFIA",
        "PUNT_FISICA",
        "PUNT_QUIMICA",
        "PUNT_LENGUAJE",
        "RECAF_PUNT_SOCIALES_CIUDADANAS",
        "RECAF_PUNT_INGLES",
        "RECAF_PUNT_LECTURA_CRITICA",
        "RECAF_PUNT_MATEMATICAS",
        "RECAF_PUNT_C_NATURALES",
        "ESTU_PUESTO",
        "PUNT_PROFUNDIZA_MATEMATICA",
        "PUNT_PROFUNDIZA_LENGUAJE",
        "PUNT_PROFUNDIZA_BIOLOGIA",
        "PUNT_PROFUNDIZA_CSOCIALES",
        "PUNT_INTERDISC_VIOLENCIAYSOC",
        "PUNT_INTERDISC_MEDIOAMBIENTE",
        "DESEMP_INGLES",
        "DESEMP_PROFUNDIZA_LENGUAJE",
        "DESEMP_PROFUNDIZA_MATEMATICA",
        "DESEMP_PROFUNDIZA_BIOLOGIA",
        "DESEMP_PROFUNDIZA_CSOCIALES",
    ]
    + notasz
    + deciles
)

labels = data[labels_names]
features = data.drop(vars_2_omit, axis=1)

scaler_features = pickle.load(open("../models/scaler_features_20141.pkl", "rb"))
scaler_labels = pickle.load(open("../models/scaler_labels_20141.pkl", "rb"))

X = scaler_features.transform(features)
Y = scaler_labels.transform(labels)

# List the Z models names

files = os.listdir(os.path.join(currpath, "../models/"))
models = [
    i for i in files if i.startswith("icfes_20141_NOTA_Z") and i.endswith(".model")
]

bst = xgb.Booster({"nthread": 4})  # Init model
notaz_pred = pd.DataFrame(columns=models)
for model in models:
    bst.load_model(os.path.join(currpath, "../models/", model))  # Load data
    features_samples = xgb.DMatrix(X, label=Y)
    note_Z_predictions = bst.predict(features_samples)
    notaz_pred[model] = note_Z_predictions

features_icfes_20141 = features.columns
data_concat = pd.concat(
    [
        data[features_icfes_20141],
        data[notasz],
        df_FTP_SABER11_20141["COLE_CODIGO_ICFES"],
        notaz_pred[models],
    ],
    axis=1,
    join="inner",
)

notasz_error_names = ["ABS_ERR_" + notaz for notaz in notasz]
notasz_error = pd.DataFrame(
    np.abs(data_concat[notasz].values - data_concat[models].values),
    columns=notasz_error_names,
)

final_info = pd.concat([data_concat, notasz_error], axis=1)

for notasz_error in notasz_error_names:
    df = (
        final_info[["COLE_CODIGO_ICFES", notasz_error]]
        .groupby("COLE_CODIGO_ICFES")
        .mean()
        .dropna()
    )
    df.sort_values(by=[notasz_error], inplace=True)
    df[:40].plot.bar(figsize=(10, 5))
    plt.show()
