from functools import partial
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from scipy import interpolate
from scipy.optimize import fsolve
import umap
from pickle import dump, load

#Import data
dir_data_for_training = "../../../data/Data_For_Training/"
dir_models = "../models/"
type_of_data_filling = ["em","mice","knn"]
resultados = ['DESEMP_INGLES', 'PUNT_MATEMATICAS', 'PUNT_CIENCIAS_SOCIALES', 
            'PUNT_INGLES', 'PUNT_BIOLOGIA', 'PUNT_FILOSOFIA',
 'PUNT_FISICA', 'PUNT_QUIMICA', 'PUNT_LENGUAJE', 'RECAF_PUNT_SOCIALES_CIUDADANAS',
 'RECAF_PUNT_INGLES', 'RECAF_PUNT_LECTURA_CRITICA', 'RECAF_PUNT_MATEMATICAS',
 'RECAF_PUNT_C_NATURALES', 'ESTU_PUESTO', 
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_MATEMATICAS',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_CIENCIAS_SOCIALES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_INGLES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_BIOLOGIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FILOSOFIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FISICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_QUIMICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_LENGUAJE',
 'NOTA_Z_COLE_CODIGO_ICFES_ESTU_PUESTO',
 'DECILE_PUNT_MATEMATICAS', 'DECILE_PUNT_CIENCIAS_SOCIALES',
 'DECILE_PUNT_INGLES', 'DECILE_PUNT_BIOLOGIA',
 'DECILE_PUNT_FILOSOFIA', 'DECILE_PUNT_FISICA',
 'DECILE_PUNT_QUIMICA', 'DECILE_PUNT_LENGUAJE',
 'DECILE_ESTU_PUESTO']

resultados_2_fit = ['NOTA_Z_COLE_CODIGO_ICFES_PUNT_MATEMATICAS',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_CIENCIAS_SOCIALES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_INGLES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_BIOLOGIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FILOSOFIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FISICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_QUIMICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_LENGUAJE',
 'NOTA_Z_COLE_CODIGO_ICFES_ESTU_PUESTO',
 'DECILE_PUNT_MATEMATICAS', 'DECILE_PUNT_CIENCIAS_SOCIALES',
 'DECILE_PUNT_INGLES', 'DECILE_PUNT_BIOLOGIA',
 'DECILE_PUNT_FILOSOFIA', 'DECILE_PUNT_FISICA',
 'DECILE_PUNT_QUIMICA', 'DECILE_PUNT_LENGUAJE',
 'DECILE_ESTU_PUESTO']

#Cargamos la información
element = type_of_data_filling[0]
print('tipo de llenado ',element)
data = pd.read_csv(dir_data_for_training + "icfes_20141_nn_"+element+".csv")
data = data.set_index("Unnamed: 0")

#Dividimos en caracteristicas y resultados
data_results = data[resultados_2_fit]
data_features = data.drop(resultados,axis=1)

#Dividimos los datos en test y train y guardamos los datos
x_train, x_test, y_train, y_test = train_test_split(data_features,data_results,test_size=0.2, 
                                                    random_state=42)

x_train_df = pd.DataFrame(data=x_train,columns=data_features.columns.to_list())
y_train_df = pd.DataFrame(data=y_train,columns=data_results.columns.to_list())
x_test_df = pd.DataFrame(data=x_test,columns=data_features.columns.to_list())
y_test_df = pd.DataFrame(data=y_test,columns=data_results.columns.to_list())

train_df = pd.concat([x_train_df,y_train_df],axis=1)
test_df = pd.concat([x_test_df,y_test_df],axis=1)

train_df.to_csv(dir_data_for_training + "2014_1_nn_train.csv")
test_df.to_csv(dir_data_for_training + "2014_1_nn_test.csv")

#Escalamos los datos de entrenamiento
scaler_1 = StandardScaler()
scaler_1.fit(x_train) 
x_train_scaled = scaler_1.transform(x_train)

#Guardamos el primer escalador
name = dir_models + 'scaler_2014_1_nn_'+element+'_1.pkl'
dump(scaler_1, open(name, 'wb'))

#perform PCA eliminate linear dependences
threshold = 0.95
explained_variance = []
amount_of_components = []
for k in range(data_features.shape[1]//10):
    pca = PCA(n_components=k*10) #Entrenar la reduccion de dimensionalidad PCA  
    pca.fit(x_train_scaled) # obtener los componentes principales
    amount_of_components.append(k*10)
    explained_variance.append(sum(pca.explained_variance_ratio_))
    print("Componentes ", k*10," explained variance ",sum(pca.explained_variance_ratio_))
    if sum(pca.explained_variance_ratio_) > threshold:
        break
        
#Encontrar la cantidad necesaria de componentes del PCA        
tck = interpolate.splrep(amount_of_components,explained_variance, s=0)
xnew = np.arange(0, max(amount_of_components)+10, 1)
ynew = interpolate.splev(xnew, tck, der=0)

def f(x,value,tck):
    return interpolate.splev(x,tck,der=0)-value

starting_guess = 100
pca_comp = int(fsolve(partial(f,value=threshold,tck=tck),starting_guess))
print("Cantidad de componentes ", pca_comp)

print("Reducción de dimensionalidad con PCA")
# Hacemos la transformación con PCA
pca_trans = PCA(n_components=pca_comp) 
pca_trans.fit(x_train_scaled) 
x_train_pca = pca_trans.transform(x_train_scaled)

# Guardamos el modelo de PCA
filename = dir_models + 'pca_2014_1_'+element+'.pkl'
dump(pca_trans, open(filename, 'wb'))

#Escalamos los datos luego de aplicar PCA
scaler_2 = StandardScaler()
scaler_2.fit(x_train_pca) 
x_train_pca_scaled = scaler_2.transform(x_train_pca)

#Guardamos el segundo escalador
name = dir_models + 'scaler_2014_1_nn_'+element+'_2.pkl'
dump(scaler_2, open(name, 'wb'))

#Creamos una función para transformar los datos
def transform_data(scaler_1,pca_trans,scaler_2,umap_trans,data):
    data_scaled = scaler_1.transform(data)
    data_pca = pca_trans.transform(data_scaled)
    data_pca_scaled = scaler_2.transform(data_pca)
    data_umap = umap_trans.transform(data_pca_scaled) 
    return data_umap

#Creamos los escaladores de umap que ajustan las caracteristicas deseadas
for result in resultados_2_fit:

    print("Reducción de dimensionalidad supervisada de UMAP para ", result)
    #Realizamos reducción de dimensionalidad supervisada con UMAP y guardamos el modelo
    n_components=30
    mapper_umap_trained = umap.UMAP(metric="l1",random_state=1,n_neighbors=20,n_components=n_components,
                    min_dist=0.2).fit(x_train_pca_scaled,y=y_train[result])

    f_name = dir_models + '2014_1_nn_'+element+'_'+result+'.umap'
    dump(mapper_umap_trained, open(f_name, 'wb'))
    
    print("Guardar los datos con caracteristicas reducidas para ", result)
    # Transformamos los datos
    x_train_umap = mapper_umap_trained.embedding_ 
    x_test_umap = transform_data(scaler_1,pca_trans,scaler_2,mapper_umap_trained,x_test_df)
    
    # Los volvemos dataframes y los guardamos
    cols = []
    for ii in range(n_components):
        cols.append("feature_"+str(ii))
    x_train_umap_df = pd.DataFrame(data = x_train_umap, columns = cols, index = y_train_df.index)
    x_test_umap_df = pd.DataFrame(data = x_test_umap, columns = cols, index = y_test_df.index)
    
    train_umap_df = pd.concat([x_train_umap_df,y_train_df],axis=1)
    test_umap_df = pd.concat([x_test_umap_df,y_test_df],axis=1)
    
    f_name = "2014_1_nn_train_"+result+".csv"
    train_umap_df.to_csv(dir_data_for_training + f_name)
    f_name = "2014_1_nn_test_"+result+".csv"
    test_umap_df.to_csv(dir_data_for_training + f_name)
    
    
    