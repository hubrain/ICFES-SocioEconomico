import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from pickle import load, dump
import umap

# Directorios importantes
dir_data_for_training = "../../../data/Data_For_Training/"
dir_models = "../models/"

### EL TIPO DE LLENADO QUE SE DESEA UTILIZAR
type_of_data_filling = ["em","mice","knn"]
### Es preferible usar el llenado knn, el archivo de esos datos está en el PC de GOIC, es por esto que antes de utilizarlo es recomendable testear que el llenado se haya realizado correctamente
element = type_of_data_filling[1]

### EL RESULTADO QUE SE DESEA AJUSTAR
result = "DECILE_PUNT_MATEMATICAS"

# Importamos los datos, los escaladores y los modelos de reducción de dimensionalidad
data_train_path = dir_data_for_training + "2014_1_nn_train.csv"
data_test_path = dir_data_for_training + "2014_1_nn_test.csv"
train_data = pd.read_csv(data_train_path)
train_data = train_data.set_index("Unnamed: 0")
test_data = pd.read_csv(data_test_path)
test_data = test_data.set_index("Unnamed: 0")


name = dir_models + 'scaler_2014_1_nn_'+element+'_1.pkl'
scaler_1 = load(open(name, 'rb'))

name = dir_models + 'scaler_2014_1_nn_'+element+'_2.pkl'
scaler_2 = load(open(name, 'rb'))

filename = dir_models + 'pca_2014_1_'+element+'.pkl'
pca_trans = load(open(filename, 'rb'))

f_name = dir_models + '2014_1_nn_'+element+'_'+result+'.umap'
try:
    umap_trans = load((open(f_name, 'rb')))
    print("Se importaron exitosamente los modelos")
except:
    raise ModuleNotFoundError("Parece que no sirvió el debbuging que hicimos. El modelo umap no pudo ser cargado de forma correcta")
    
#Dividir los datos en caracteristicas y resultados
resultados = ['NOTA_Z_COLE_CODIGO_ICFES_PUNT_MATEMATICAS',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_CIENCIAS_SOCIALES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_INGLES',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_BIOLOGIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FILOSOFIA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_FISICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_QUIMICA',
 'NOTA_Z_COLE_CODIGO_ICFES_PUNT_LENGUAJE',
 'NOTA_Z_COLE_CODIGO_ICFES_ESTU_PUESTO',
 'DECILE_PUNT_MATEMATICAS',
 'DECILE_PUNT_CIENCIAS_SOCIALES',
 'DECILE_PUNT_INGLES',
 'DECILE_PUNT_BIOLOGIA',
 'DECILE_PUNT_FILOSOFIA',
 'DECILE_PUNT_FISICA',
 'DECILE_PUNT_QUIMICA',
 'DECILE_PUNT_LENGUAJE',
 'DECILE_ESTU_PUESTO']

y_train = train_data[resultados]
x_train = train_data.drop(resultados,axis=1)
y_test = test_data[resultados]
x_test = test_data.drop(resultados,axis=1)

def transform_data(scaler_1,pca_trans,scaler_2,umap_trans,data):
    data_scaled = scaler_1.transform(data)
    data_pca = pca_trans.transform(data_scaled)
    data_pca_scaled = scaler_2.transform(data_pca)
    data_umap = umap_trans.transform(data_pca_scaled) 
    return data_umap

# Transformamos los datos
x_train_umap = transform_data(scaler_1,pca_trans,scaler_2,umap_trans,x_train) 
x_test_umap = transform_data(scaler_1,pca_trans,scaler_2,umap_trans,x_test) 

print("La carga de los modelos sirve, al igual que la tranformación de los datos.")
    

#print(shape)

#print(type(umap_trans))
#print("Test passed")