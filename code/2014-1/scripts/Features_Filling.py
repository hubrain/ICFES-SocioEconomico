import pandas as pd
import numpy as np
import os
import json
import sys

# Open data pandas read csv
data_for_training = "../../../data/Data_For_Training/"
data = pd.read_csv(data_for_training + "icfes_20141.csv")
data = data.set_index("Unnamed: 0")

porcentaje = 0.35
thresh = int(data.shape[0]*porcentaje)
print("filas totales", data.shape[0], " requerimos ", thresh, "filas llenas")
print("cantidad de columnas ",len(set(data.columns.to_list()).difference(data.dropna(axis='columns',thresh=thresh).columns.to_list())))
low_filled_2 = list(set(data.columns.to_list()).difference(data.dropna(axis='columns',thresh=thresh).columns.to_list()))
low_filled_2

names_nota_z = [col for col in data.columns.to_list() if "NOTA_Z" in col]
df_nn = data.drop(low_filled_2, axis=1)
print("Incluir Nota Z con valores NaN")
print("Cantidad de columnas ", df_nn.shape[1], ", Cantidad de filas", df_nn.shape[0])
print("Eliminar Nota Z con valores NaN")
df_nn = df_nn.dropna(subset = names_nota_z)
print("Cantidad de columnas ", df_nn.shape[1], ", Cantidad de filas", df_nn.shape[0])

#Llenar los valores incompletos
print("Llenando los dataframes")

import impyute as impy
sys.setrecursionlimit(100000)
values = df_nn.values
values

print("Using the EM method")

data_em = impy.em(values)
df_nn_em = pd.DataFrame(data=data_em, columns=df_nn.columns.to_list())
df_nn_em.set_index(df_nn.index)
df_nn_em.to_csv(data_for_training + "icfes_20141_nn_em.csv")

print("Using the MICE method")

data_mice = impy.mice(values)
df_nn_mice = pd.DataFrame(data=data_mice, columns=df_nn.columns.to_list())
df_nn_mice.set_index(df_nn.index)
df_nn_mice.to_csv(data_for_training + "icfes_20141_nn_mice.csv")

print("Using the knn method")

data_knn = impy.fast_knn(values,k=30)
df_nn_knn = pd.DataFrame(data=data_knn, columns=df_nn.columns.to_list())
df_nn_knn.set_index(df_nn.index)
df_nn_knn.to_csv(data_for_training + "icfes_20141_nn_knn.csv")