import pandas as pd
import numpy as np
import os
import json

data_dir = "../../../data/unziped_data/"


# Para variable 'ESTU_ANOSCOLEGIOACTUAL'
def anoscolegio_actual(value):
    if value == "1 año":
        return 1
    elif value == "2 años":
        return 2
    elif value == "3 años":
        return 3
    elif value == "4 años":
        return 4
    elif value == "5 años":
        return 5
    elif value == "6 años":
        return 6
    elif value == "7 años":
        return 7
    elif value == "8 años":
        return 8
    else:
        return np.nan


# Para variable 'ESTU_CUANTOSCOLEGIOESTUDIO'
def cuantos_colegios(value):
    if value == "Dos colegios":
        return 0
    elif value == "Tres colegios":
        return 1
    elif value == "Cuatro colegios o mas":
        return 2
    else:
        return np.nan


# Para Variable 'ESTU_SERETIROCOLEGIO'
def retirodel_colegio(value):
    if value == "No":
        return 0
    elif value == "Si, menos de un año escolar":
        return 1
    elif value == "Si, un año escolar":
        return 2
    elif value == "SI, 2 años escolares o más":
        return 3
    else:
        return np.nan


# Para variable   'ESTU_PROGORIENTACIONVOCACIONAL'
def orientacion_vocacional(value):
    if value == "Si":
        return 1
    elif value == "No":
        return 0
    elif value == "No recibió, consultó o participó":
        return 0
    else:
        return np.nan


# Para variable 'ESTU_PROGPORBUSCANDOCARRERA'
def probuscando_carrera(value):
    if value == "Si":
        return 1
    elif value == "No":
        return 0
    elif value == "No recibió, consultó o participó":
        return 0
    else:
        return np.nan


# Para variable  'ESTU_PROGPORCOLOMBIAAPRENDE'
def procolombia_aprende(value):
    if value == "Si":
        return 1
    elif value == "No":
        return 0
    elif value == "No recibió, consultó o participó":
        return 0
    else:
        return np.nan


def parse_nacionalidad(val):
    if val == "COLOMBIA":
        return 1
    elif val == val:
        return 0
    else:
        return np.nan


#### Joao
def parse_desemp_ingles(val):
    if val == "A-":
        return 1
    elif val == "A1":
        return 2
    elif val == "A2":
        return 3
    elif val == "B+":
        return 4
    elif val == "B1":
        return 5
    else:
        return np.nan


def parse_desemp_profundizacion(val):
    if val == "GB":
        return 1
    elif val == "I":
        return 2
    elif val == "II":
        return 3
    elif val == "III":
        return 4
    else:
        return np.nan


### Leoneli
def parse_espera_ingresar_edu_sup(val):
    if val == "No voy a continuar estudiando":
        return 0
    elif val == "Es poco probable que ingrese a un programa de Educación Superior":
        return 1
    elif val == "Es probable que ingrese a un programa de Educación Superior":
        return 2
    elif val == "Con toda seguridad voy a ingresar a un programa de Educación Superior":
        return 3
    else:
        return np.nan


def parse_punt_esperado(val):
    if val == "Muy bajo (Entre 0 y 30 puntos)":
        return 0
    elif val == "Bajo (Entre 30,01 puntos y 50,00 puntos)":
        return 1
    elif val == "Medio (Entre 50,01 puntos y 70,00 puntos)":
        return 2
    elif val == "Alto (Entre 70,01 o más)":
        return 3
    else:
        return np.nan


def parse_salar_esperado(val):
    if (
        val
        == "Menos de 1 Salario Mínimo Legal Vigente (SMLV) (En 2013 el salario mínimo es de $589.500 pesos)"
    ):
        return 0
    elif val == "Entre 1 y 2 SMLV":
        return 1
    elif val == "Entre 3 y 4 SMLV":
        return 2
    elif val == "Entre 5 y 7 SMLV":
        return 3
    elif val == "Entre 8 y 10 SMLV":
        return 4
    elif val == "Más de 10 SMLV":
        return 5
    else:
        return np.nan


def parse_cole_naturaleza(val):
    if val == "NO OFICIAL":
        return 1
    elif val == "OFICIAL":
        return 0
    else:
        return np.nan


def parse_S_N(val):
    if val == "S":
        return 1
    elif val == "N":
        return 0
    else:
        return np.nan


def parse_cole_area_ubic(val):
    if val == "URBANO":
        return 1
    elif val == "RURAL":
        return 0
    else:
        return np.nan


### Volodya


def parse_numbers(val):
    if val == "Tres":
        return 3
    elif val == "Dos":
        return 2
    elif val == "Cuatro":
        return 4
    elif val == "Cinco":
        return 5
    elif val == "Uno":
        return 1
    elif val == "Seis":
        return 6
    elif val == "Siete":
        return 7
    elif val == "Ocho":
        return 8
    elif val == "Una":
        return 1
    elif val == "Diez":
        return 10
    elif val == "Once":
        return 11
    elif val == "Doce o más":
        return 12
    elif val == "Diez o más":
        return 10
    elif val == "Nueve":
        return 9
    elif val == "Ninguno":
        return 0
    else:
        return np.nan


def parse_Si_No(val):
    if val == "Si":
        return 1
    elif val == "No":
        return 0
    else:
        return np.nan


def parse_fami_ingresofamiliarmensual(val):
    if val == "Entre 1 y menos de 2 SMLV":
        return 1.5
    elif val == "Entre 2 y menos de 3 SMLV":
        return 2.5
    elif val == "10 o más SMLV":
        return 10
    elif val == "Entre 3 y menos de 5 SMLV":
        return 4.0
    elif val == "Menos de 1 SMLV":
        return 0.7
    elif val == "Entre 5 y menos de 7 SMLV":
        return 6.0
    elif val == "Entre 7 y menos de 10 SMLV":
        return 8.5
    else:
        return np.nan


def parse_estu_trabajaactualmente(val):
    if val == "No":
        return 0
    elif val == "Si, 20 horas o más a la semana":
        return 2
    elif val == "Si, menos de 20 horas a la semana":
        return 1
    else:
        return np.nan


def parse_year(val):
    if val == "1985 o antes":
        return 1985
    else:
        try:
            return int(val)
        except:
            return np.nan


def parse_genero(val):
    if val == "M":
        return 1
    elif val == "F":
        return 0
    else:
        return np.nan


def parse_nacimiento(val, year="01-01-2014", min_year=pd.to_datetime("1985-01-01")):
    date = max(pd.to_datetime(val, dayfirst=True, errors="coerce"), min_year)
    return (pd.to_datetime(year, dayfirst=True) - date).days


def parse_area_reside(val):
    if val == "Cabecera Municipal":
        return 1
    elif val == "Area Rural":
        return 0
    else:
        return np.nan


def parse_pension_colegio(val):
    if val == "250.000 o más":
        return 5
    elif val == "Entre 150.000 y menos de 250.000":
        return 4
    elif val == "Entre 120.000 y menos de 150.000":
        return 3
    elif val == "Entre 87.000 y menos de 120.000":
        return 2
    elif val == "Menos de 87.000":
        return 1
    elif val == "No paga Pensión":
        return 0
    else:
        return np.nan


def parse_veces_examen(val):
    if val == "Ninguna vez":
        return 0
    elif val == "Una vez":
        return 1
    elif val == "Dos veces":
        return 2
    elif val == "Tres veces o más":
        return 3
    else:
        return np.nan


def parse_sisben(val):
    if val == "No está clasificada por el SISBEN":
        return 0
    elif val == "Nivel 1":
        return 1
    elif val == "Nivel 2":
        return 2
    elif val == "Nivel 3":
        return 3
    elif val == "Esta clasificada en otro nivel del SISBEN":
        return 4
    else:
        return np.nan


def parse_estrato(val):
    if val == "Estrato 1":
        return 1
    elif val == "Estrato 2":
        return 2
    elif val == "Estrato 3":
        return 3
    elif val == "Estrato 4":
        return 4
    elif val == "Estrato 5":
        return 5
    elif val == "Estrato 6":
        return 6
    else:
        return np.nan


def parse_mun_X(codigo, dictX):
    if codigo in dictX.keys():
        return dictX[codigo]
    else:
        return np.nan


def parse_tipo_carrera_deseada(val):
    if val == "Ninguna":
        return 0
    elif val == "Técnica":
        return 1
    elif val == "Tecnológica":
        return 2
    elif val == "Profesional":
        return 3
    else:
        return np.nan


# For Variable 'ESTU_NACIONALIDAD', 'ESTU_PAIS_RESIDE'
def parse_nacionalidad(value):
    if value == "Colombia":
        return 1
    elif value != np.NaN:
        return 0
    else:
        return np.NaN


# For variable 'ESTU_ETNIA'
def parse_etnia(value):
    if value == "Ninguno":
        return 0
    elif value != np.NaN:
        return 1
    else:
        return np.NaN


# For variable 'ESTU_NSE_INDIVIDUAL'
def parse_NSE(val):
    """Nivel Socio Económico"""
    if val == "NSE1":
        return 1
    elif val == "NSE2":
        return 2
    elif val == "NSE3":
        return 3
    elif val == "NSE4":
        return 4
    else:
        return np.nan


# For parsing 'ESTU_PROGRAMADESEADO'
def parsing_carreras(
    carrera, name_dic="../../../data/Diccionario_carreras/dic_carreras_facultades.json"
):
    with open(name_dic, "r") as f:
        dictionary = json.load(f)
    try:
        ss = dictionary[carrera]
    except:
        ss = np.nan
    return ss


# For Parsing 'FAMI_PISOSHOGAR'
def parse_pisos_hogar(val):
    if val == "Tierra, arena":
        return 0
    elif val == "Cemento, gravilla, ladrillo":
        return 1
    elif val == "Madera burda, tabla, tablón":
        return 2
    elif val == "Madera pulida, baldosa, tableta, mármol, alfombra":
        return 3
    else:
        return np.nan


# For Parsing 'FAMI_EDUCACIONPADRE' y 'FAMI_EDUCACIONMADRE'
def parse_nivel_educacion(val):
    if val == "No sabe" or val == "No aplica" or val == "No Aplica":
        return [0, 1]
    else:
        if val == "Ninguno":
            return [0, 0]
        elif val == "Primaria incompleta":
            return [1, 0]
        elif val == "Primaria completa":
            return [2, 0]
        elif val == "Secundaria (Bachillerato) incompleta":
            return [3, 0]
        elif val == "Secundaria (Bachillerato) completa":
            return [4, 0]
        elif val == "Técnica o tecnológica incompleta":
            return [5, 0]
        elif val == "Técnica o tecnológica completa":
            return [6, 0]
        elif val == "Educación profesional incompleta":
            return [7, 0]
        elif val == "Educación profesional completa":
            return [8, 0]
        elif val == "Postgrado":
            return [9, 0]
        else:
            return [np.nan, np.nan]


# Jerarquia de ocupaciones diseñada por Oscar y Brayan
def parse_ocupacion_padres(val):
    """
    Parsing the type of ocupation of the parents to a ranking based on skill level,
    social status and the Clasificación Internacional Uniforme de Ocupaciones.
    Adaptación Colombia (CIUO A.C.)
    ______________________________________________________________________________________
    Parameters:
        val: str containing the answer to the question
    Returns:
        Lista: List with four items
            Lista[0] ranking given by Oscar and Brayan
            Lista[1] Clasificación Internacional Uniforme de Ocupaciones. Adaptación Colombia (CIUO A.C.)
            Lista[2] Skill level
            Lista[3] Social status ranking inside each skill level (Bajo = 1, Medio = 2, Medio-Alto = 3, Alto = 4)
    """
    if val == "Pensionado":
        return [1, np.nan, 0, 1]
    elif val == "Otra actividad u ocupación":
        return [2, 1, 2, 1]
    elif val == "Hogar":
        return [3, 1, 1, 1]
    elif val == "Trabajador por cuenta propia":
        return [4, 1, 2, 1]
    elif val == "Empleado obrero u operario":
        return [5, 3, 2, 1]
    elif val == "Pequeño empresario":
        return [6, 5, 2, 2]
    elif val == "Empleado de nivel auxiliar o administrativo":
        return [7, 6, 2, 2]
    elif val == "Profesional independiente" or val == "Profesional Independiente":
        return [8, 7, 3, 3]
    elif val == "Empleado de nivel técnico o profesional":
        return [9, 8, 4, 3]
    elif val == "Empleado de nivel directivo":
        return [10, 9, 4, 4]
    elif val == "Empleado con cargo como director o gerente general":
        return [11, 9, 4, 4]
    elif val == "Empresario":
        return [12, 9, 4, 4]
    else:
        return [np.nan, np.nan, np.nan, np.nan]


#### Calcular la nota Z


def Calculate_Nota_Z(category, puntajes, df):
    usefull_cols = puntajes.copy()
    usefull_cols.append(category)
    df_notas = df[usefull_cols]
    name = "NOTA_Z_" + category + "_"

    # Create columns to store Z scores and fill them initially with nans
    for puntaje in puntajes:
        df_notas.loc[:, name + puntaje] = np.nan

    colegios_counter = 0
    for groupname, group in df_notas.groupby(category):
        # Check if group has more than 20 students:
        if group.shape[0] < 20:
            continue
        else:
            colegios_counter += 1

        for puntaje in puntajes:
            mu = group[puntaje].mean()
            std = group[puntaje].std()
            if std != 0:
                df.loc[group.index, name + puntaje] = (
                    group[puntaje].to_numpy() - mu
                ) / std
            else:
                continue

    print("Colegios contados con mínimo 20 estudiantes:", colegios_counter)
    nota_z_names = [name + puntaje for puntaje in puntajes]
    return df, nota_z_names


def calculate_decile(puntaje, df):
    np.random.seed(0)
    maxval = df[puntaje].max()
    minval = df[puntaje].min()
    size = df[puntaje].shape[0]
    rangeval = (maxval - minval) / 10000
    df.loc[:, "DECILE_" + puntaje] = pd.qcut(
        df[puntaje] + np.random.uniform(0, rangeval, size), 1000, labels=False
    )
    return df


def compute_all_deciles(puntajes, df):
    col_names = []
    for puntaje in puntajes:
        df = calculate_decile(puntaje, df)
        col_names.append("DECILE_" + puntaje)
    return df, col_names


# Diccionario de las funciones asociadas a alguna columna
column_2_func = {}

column_2_func["FAMI_CUARTOSHOGAR"] = parse_numbers
column_2_func["FAMI_TIENE_CELULAR"] = parse_Si_No
column_2_func["FAMI_TELEFONO"] = parse_Si_No
column_2_func["FAMI_INGRESOFMILIARMENSUAL"] = parse_fami_ingresofamiliarmensual
column_2_func["ESTU_TRABAJAACTUALMENTE"] = parse_estu_trabajaactualmente
column_2_func["ESTU_RECIBESALARIO"] = parse_Si_No
column_2_func["ESTU_ANOSPREESCOLAR"] = parse_numbers
column_2_func["ESTU_ANOMATRICULAPRIMERO"] = parse_year
column_2_func["ESTU_ANOTERMINOQUINTO"] = parse_year
column_2_func["ESTU_ANOMATRICULASEXTO"] = parse_year
column_2_func["ESTU_ANTECEDENTES"] = parse_Si_No
column_2_func["ESTU_REPROBOPRIMERO"] = parse_Si_No
column_2_func["ESTU_REPROBOSEGUNDO"] = parse_Si_No
column_2_func["ESTU_REPROBOTERCERO"] = parse_Si_No
column_2_func["ESTU_REPROBOCUARTO"] = parse_Si_No
column_2_func["ESTU_REPROBOQUINTO"] = parse_Si_No
column_2_func["ESTU_REPROBOSEXTO"] = parse_Si_No
column_2_func["ESTU_REPROBOSEPTIMO"] = parse_Si_No
column_2_func["ESTU_REPROBOOCTAVO"] = parse_Si_No
column_2_func["ESTU_REPROBONOVENO"] = parse_Si_No
column_2_func["ESTU_REPROBODECIMO"] = parse_Si_No
column_2_func["ESTU_REPROBOONCEMAS"] = parse_Si_No
column_2_func["ESTU_ANOSCOLEGIOACTUAL"] = anoscolegio_actual
column_2_func["ESTU_CUANTOSCOLEGIOESTUDIO"] = cuantos_colegios
column_2_func["ESTU_SERETIROCOLEGIO"] = retirodel_colegio
column_2_func["ESTU_TIPOCARRERADESEADA"] = parse_tipo_carrera_deseada
column_2_func["ESTU_INSTPORPRESTIGIO"] = parse_Si_No
column_2_func["FAMI_TIENEINTERNET"] = parse_Si_No
column_2_func["FAMI_TIENECOMPUTADOR"] = parse_Si_No
column_2_func["FAMI_TIENELAVADORA"] = parse_Si_No
column_2_func["FAMI_TIENEMICROONDAS"] = parse_Si_No
column_2_func["FAMI_TIENEHORNO"] = parse_Si_No
column_2_func["FAMI_TIENESERVICIOTV"] = parse_Si_No
column_2_func["FAMI_TIENEAUTOMOVIL"] = parse_Si_No
column_2_func["FAMI_TIENEDVD"] = parse_Si_No
column_2_func["FAMI_TIENE_NEVERA"] = parse_Si_No
column_2_func["FAMI_PERSONASHOGAR"] = parse_numbers
column_2_func["FAMI_CUARTOSHOGAR"] = parse_numbers
column_2_func["FAMI_NIVELSISBEN"] = parse_sisben
column_2_func["FAMI_ESTRATOVIVIENDA"] = parse_estrato
column_2_func["ESTU_VECESPRESENTOEXAMEN"] = parse_veces_examen
column_2_func["ESTU_VALORPENSIONCOLEGIO"] = parse_pension_colegio
column_2_func["ESTU_AREARESIDE"] = parse_area_reside
column_2_func["ESTU_ETNIA"] = parse_etnia
column_2_func["ESTU_FECHANACIMIENTO"] = parse_nacimiento
column_2_func["ESTU_GENERO"] = parse_genero
column_2_func["ESTU_NACIONALIDAD"] = parse_nacionalidad
column_2_func["ESTU_PAIS_RESIDE"] = parse_nacionalidad
column_2_func["ESTU_INSTPORCOSTOMATRICULA"] = parse_Si_No
column_2_func["ESTU_INSTPORUBICACION"] = parse_Si_No
column_2_func["ESTU_INSTPORUNICAQUEOFRECE"] = parse_Si_No
column_2_func["ESTU_INSTPOROPORTUNIDADES"] = parse_Si_No
column_2_func["ESTU_INSTPORAMIGOSESTUDIANDO"] = parse_Si_No
column_2_func["ESTU_INSTPOROTRARAZON"] = parse_Si_No
column_2_func["ESTU_PROGPORINTERESPERSONAL"] = parse_Si_No
column_2_func["ESTU_PROGPORTRADICIONFAMILIAR"] = parse_Si_No
column_2_func["ESTU_PROGPORMEJORARPOSICSOCIAL"] = parse_Si_No
column_2_func["ESTU_PROGPORINFLUENCIAALGUIEN"] = parse_Si_No
column_2_func["ESTU_EXPECTATIVAS"] = parse_Si_No
column_2_func["COLE_BILINGUE"] = parse_S_N
column_2_func["COLE_SEDE_PRINCIPAL"] = parse_S_N
column_2_func["ESTU_PROGORIENTACIONVOCACIONAL"] = orientacion_vocacional
column_2_func["ESTU_PROGPORBUSCANDOCARRERA"] = probuscando_carrera
column_2_func["ESTU_PROGPORCOLOMBIAAPRENDE"] = procolombia_aprende
column_2_func["ESTU_INGRESAR_PROG_EDUSUPERIOR"] = parse_espera_ingresar_edu_sup
column_2_func["ESTU_PUNT_ESPERADO_LENGUAJE"] = parse_punt_esperado
column_2_func["ESTU_PUNT_ESPERADO_MATEMATICAS"] = parse_punt_esperado
column_2_func["ESTU_PUNT_ESPERADO_INGLES"] = parse_punt_esperado
column_2_func["ESTU_SALARIO_ESPERADOBACHILLER"] = parse_salar_esperado
column_2_func["ESTU_SALARIO_ESPERADOTECNICO"] = parse_salar_esperado
column_2_func["ESTU_SALARIO_ESPERADOPROFESI"] = parse_salar_esperado
column_2_func["COLE_NATURALEZA"] = parse_cole_naturaleza
column_2_func["COLE_AREA_UBICACION"] = parse_cole_area_ubic
column_2_func["ESTU_PRIVADO_LIBERTAD"] = parse_S_N
column_2_func["DESEMP_INGLES"] = parse_desemp_ingles
column_2_func["ESTU_NSE_INDIVIDUAL"] = parse_NSE
column_2_func["ESTU_PROGRAMADESEADO"] = parsing_carreras
column_2_func["FAMI_PISOSHOGAR"] = parse_pisos_hogar
column_2_func["FAMI_EDUCACIONPADRE"] = parse_nivel_educacion
column_2_func["FAMI_EDUCACIONMADRE"] = parse_nivel_educacion
column_2_func["FAMI_OCUPACIONPADRE"] = parse_ocupacion_padres
column_2_func["FAMI_OCUPACIONMADRE"] = parse_ocupacion_padres

# lista de las columnas que se van a convertir a one-hot con pd.get_dummies
columns_with_get_dummies = [
    "ESTU_DEPTO_RESIDE",
    "COLE_GENERO",
    "COLE_CALENDARIO",
    "COLE_CARACTER",
    "COLE_JORNADA",
    "COLE_DEPTO_UBICACION",
    "ESTU_DEPTO_PRESENTACION",
]

# lista de las columnas que se van a convertir en float
numeric_var = [
    "ESTU_TOTALALUMNOSCURSO",
    "PUNT_LECTURA_CRITICA",
    "DECIL_LECTURA_CRITICA",
    "PUNT_MATEMATICAS",
    "DECIL_MATEMATICAS",
    "PUNT_C_NATURALES",
    "DECIL_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "DECIL_SOCIALES_CIUDADANAS",
    "PUNT_RAZONA_CUANTITATIVO",
    "DECIL_RAZONA_CUANTITATIVO",
    "PUNT_COMP_CIUDADANA",
    "DECIL_COMP_CIUDADANA",
    "PUNT_INGLES",
    "DECIL_INGLES",
    "PUNT_GLOBAL",
    "ESTU_PUESTO",
    "ESTU_INSE_INDIVIDUAL",
]

print("Reading data: ")

df = pd.read_csv(
    os.path.join(data_dir, "FTP_SABER11_20142.TXT"),
    sep="¬",
    header=0,
    encoding="utf-8-sig",
)

# Agregar la nota z por colegio y los mililes
puntajes = [
    "PUNT_MATEMATICAS",
    "PUNT_LECTURA_CRITICA",
    "PUNT_C_NATURALES",
    "PUNT_SOCIALES_CIUDADANAS",
    "PUNT_RAZONA_CUANTITATIVO",
    "PUNT_COMP_CIUDADANA",
    "PUNT_INGLES",
    "PUNT_GLOBAL",
    "ESTU_PUESTO",
]

print("Adding Z Score and mililes:")

df, names_nota_z = Calculate_Nota_Z("COLE_CODIGO_ICFES", puntajes, df)
df, names_deciles = compute_all_deciles(puntajes, df)
numeric_var = numeric_var + names_nota_z + names_deciles

codigos = [
    "ESTU_COD_RESIDE_MCPIO",
    "COLE_COD_MCPIO_UBICACION",
    "ESTU_COD_MCPIO_PRESENTACION",
]

to_keep = list(column_2_func.keys()) + columns_with_get_dummies + numeric_var
df_vars = df[to_keep]
df_vars = pd.get_dummies(df_vars, columns=columns_with_get_dummies, drop_first=True)

print("Applying parsing functions:")
for col, func in column_2_func.items():
    df_vars[col] = df_vars[col].apply(lambda x: func(x))

# Some columns are parsed to a list of values, so the values inside those lists are parsed to different columns
## Educacion de los padres
df_vars[
    ["FAMI_EDUCACIONMADRE_Nivel_Educativo", "FAMI_EDUCACIONMADRE_No_Aplica_No_Sabe"]
] = pd.DataFrame(df_vars.FAMI_EDUCACIONMADRE.tolist(), index=df_vars.index)
df_vars[
    ["FAMI_EDUCACIONPADRE_Nivel_Educativo", "FAMI_EDUCACIONPADRE_No_Aplica_No_Sabe"]
] = pd.DataFrame(df_vars.FAMI_EDUCACIONPADRE.tolist(), index=df_vars.index)
df_vars = df_vars.drop(["FAMI_EDUCACIONPADRE", "FAMI_EDUCACIONMADRE"], axis=1)
## Trabajo o labor de los padres
df_vars[
    [
        "FAMI_OCUPACIONPADRE_ranking",
        "FAMI_OCUPACIONPADRE_CIUO",
        "FAMI_OCUPACIONPADRE_skill_level",
        "FAMI_OCUPACIONPADRE_social_status",
    ]
] = pd.DataFrame(df_vars.FAMI_OCUPACIONPADRE.tolist(), index=df_vars.index)
df_vars[
    [
        "FAMI_OCUPACIONMADRE_ranking",
        "FAMI_OCUPACIONMADRE_CIUO",
        "FAMI_OCUPACIONMADRE_skill_level",
        "FAMI_OCUPACIONMADRE_social_status",
    ]
] = pd.DataFrame(df_vars.FAMI_OCUPACIONMADRE.tolist(), index=df_vars.index)
df_vars = df_vars.drop(["FAMI_OCUPACIONPADRE", "FAMI_OCUPACIONMADRE"], axis=1)

# Include information contained in the dictionaries about the towns where the children live
file_mun1 = "dic_mun_gradoimportancia.json"
file_mun2 = "dic_mun_pesorelativo.json"
file_mun3 = "dic_mun_poblacion.json"
file_mun4 = "dic_mun_valoragregado.json"

files_mun = [file_mun1, file_mun2, file_mun3, file_mun4]


def extract_dict(json_name, dicc_directory):
    with open(dicc_directory + json_name, "r") as file:
        json_dict = json.load(file)
    return json_dict


def num2str(num):
    try:
        return str(int(num))
    except:
        return str(num)


for col in [cod for cod in codigos if "MCPIO" in cod]:
    for file in files_mun:
        dictX = extract_dict(file, "../../../data/Diccionarios_mun/")
        df_vars[col + file[:-5]] = df[col].apply(
            lambda x: parse_mun_X(num2str(x), dictX)
        )

df_vars = pd.get_dummies(df_vars, columns=["ESTU_PROGRAMADESEADO"])

data_for_training = "../../../data/Data_For_Training/"

df_vars.to_csv(data_for_training + "icfes_20142.csv", index=False)

for name in names_nota_z + names_deciles:
    print(name, df_vars[name].isna().sum() / df_vars[name].shape[0])
