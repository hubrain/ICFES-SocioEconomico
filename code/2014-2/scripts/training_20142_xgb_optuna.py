import pandas as pd
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from functools import partial
from sklearn.metrics import confusion_matrix, roc_curve, auc
import json
import pickle
import optuna
from functools import partial
from sklearn.metrics import mean_squared_error

# Functions to have the best hyperparameters
def pre_objective(trial, dtrain, dtest):
    y_train = dtrain.get_label()
    y_test = dtest.get_label()

    params = {
        "verbosity": 1,
        "nthread": 64,
        "random_state": 0,
        "objective": "reg:squarederror",
        "booster": "gbtree",
        "tree_method": "hist",
        "eval_metric": "rmse",
        "max_depth": trial.suggest_int("max_depth", 2, 15),
        "learning_rate": trial.suggest_loguniform("learning_rate", 1e-4, 10),
        "gamma": trial.suggest_loguniform("gamma", 1e-1, 100),
        "min_child_weight": trial.suggest_int("min_child_weight", 0, 100),
        "max_delta_step": trial.suggest_int("max_delta_step", 0, 100),
        "subsample": trial.suggest_uniform("subsample", 0.5, 1),
        "colsample_bytree": trial.suggest_uniform("colsample_bytree", 0.5, 1),
        "colsample_bylevel": trial.suggest_uniform("colsample_bylevel", 0.5, 1),
        "colsample_bynode": trial.suggest_uniform("colsample_bynode", 0.5, 1),
        "reg_alpha": trial.suggest_loguniform("reg_alpha", 1e-8, 1e2),
        "reg_lambda": trial.suggest_loguniform("reg_lambda", 1e-8, 1e2),
    }

    num_round = trial.suggest_int("num_round", 10, 500)

    try:
        bst = xgb.train(
            params,
            dtrain,
            num_round,
            evals=[(dtest, "eval"), (dtrain, "train")],
            early_stopping_rounds=10,
            verbose_eval=False,
        )
        preds_test = bst.predict(dtest, ntree_limit=bst.best_ntree_limit)
        preds_train = bst.predict(dtrain, ntree_limit=bst.best_ntree_limit)
        to_minimize_1 = mean_squared_error(preds_test, y_test, squared=False)
        to_minimize_2 = mean_squared_error(preds_train, y_train, squared=False)
        print("Error in test: ", to_minimize_1, ", Error in train: ", to_minimize_2)
        err = to_minimize_1 + to_minimize_2 + abs(to_minimize_1 - to_minimize_2)
    except Exception as e:
        print(e)
        err = 10
    return err


def tune_classifier(dtrain, dtest, name, n_trials):
    objective = partial(pre_objective, dtrain=dtrain, dtest=dtest)
    study = optuna.create_study(
        study_name="icfes_20142_{0}".format(name),
        storage="sqlite:///../models/icfes_20142_{0}.db".format(name),
    )
    study.optimize(objective, n_trials=n_trials)
    return study.best_params


try:
    os.mkdir("../models")
except:
    pass

# Data directory
data_dir = "../../../data/unziped_data/"

# Open data with pickle
data_for_training = "../../../data/Data_For_Training/"
data = pd.read_csv(data_for_training + "icfes_20142.csv")

# lista de las columnas con las caracteristicas a predecir
notasz = [c for c in data.columns.values if c.startswith("NOTA_Z")]
deciles = [c for c in data.columns.values if c.startswith("DECILE")]
labels_names = notasz + deciles

# lista de las columnas que se van a omitir de las caracteristicas
vars_2_omit = (
    [
        "PUNT_LECTURA_CRITICA",
        "DECIL_LECTURA_CRITICA",
        "PUNT_MATEMATICAS",
        "DECIL_MATEMATICAS",
        "PUNT_C_NATURALES",
        "DECIL_C_NATURALES",
        "PUNT_SOCIALES_CIUDADANAS",
        "DECIL_SOCIALES_CIUDADANAS",
        "PUNT_RAZONA_CUANTITATIVO",
        "DECIL_RAZONA_CUANTITATIVO",
        "PUNT_COMP_CIUDADANA",
        "DECIL_COMP_CIUDADANA",
        "PUNT_INGLES",
        "DECIL_INGLES",
        "PUNT_GLOBAL",
        "ESTU_PUESTO",
    ]
    + deciles
    + notasz
)


labels = data[labels_names]
features = data.drop(vars_2_omit, axis=1)
print(features.columns.values)

# train/test split
features_train, features_test, labels_train, labels_test = train_test_split(
    features, labels, test_size=0.2, random_state=42
)

# Scaling the features and the labels
scale = True

if scale:
    scaler_features = StandardScaler()
    scaler_features.fit(features_train)
    scaler_labels = StandardScaler()
    scaler_labels.fit(labels_train)

else:
    scaler_features = pickle.load(open("../models/scaler_features_20142.pkl", "rb"))
    scaler_labels = pickle.load(open("../models/scaler_labels_20142.pkl", "rb"))

X_train = scaler_features.transform(features_train)
X_test = scaler_features.transform(features_test)


Y_train = scaler_labels.transform(labels_train)
Y_test = scaler_labels.transform(labels_test)

# Saving the scaler
pickle.dump(scaler_features, open("../models/scaler_features_20142.pkl", "wb"))
pickle.dump(scaler_labels, open("../models/scaler_labels_20142.pkl", "wb"))


### Best parameters for the model trained for each subject
mask_nota_z = [False if 'COLE' in col or 'DEPTO' in col or 'MCPIO' in col or "ESTABLECIMIENTO" in col else True for col in features.columns]
for ii in range(len(labels_names)):
    # Creating the train and test sets
    idx_test = np.where(~np.isnan(Y_test[:, ii]))
    idx_train = np.where(~np.isnan(Y_train[:, ii]))
    if "NOTA_Z" in labels_names[ii]:
        dtest = xgb.DMatrix(X_test[idx_test[0]][:, mask_nota_z], label=Y_test[:, ii][idx_test[0]])
        dtrain = xgb.DMatrix(X_train[idx_train[0]][:, mask_nota_z], label=Y_train[:, ii][idx_train[0]])
    else:
        dtest = xgb.DMatrix(X_test[idx_test[0]], label=Y_test[:, ii][idx_test[0]])
        dtrain = xgb.DMatrix(X_train[idx_train[0]], label=Y_train[:, ii][idx_train[0]])
    best_params = tune_classifier(dtrain, dtest, labels_names[ii], 200)
    with open(
        "../models/best_params_20142_xgboost_{0}.json".format(labels_names[ii]), "w"
    ) as f:
        json.dump(best_params, f)


### Best parameters for the model trained for each subject
for ii in range(len(labels_names)):
    with open(
        "../models/best_params_20142_xgboost_{0}.json".format(labels_names[ii]), "r"
    ) as f:
        Final_parameters = json.load(f)

    num_round = Final_parameters.pop("num_round")
    Final_parameters.update(
        {
            "verbosity": 1,
            "nthread": 64,
            "random_state": 0,
            "objective": "reg:squarederror",
            "booster": "gbtree",
            "tree_method": "hist",
            "eval_metric": "rmse",
        }
    )

    #### Final model
    X = np.vstack((X_test, X_train))
    Y = np.vstack((Y_test, Y_train))

    idx = np.where(~np.isnan(Y[:, ii]))
    if "NOTA_Z" in labels_names[ii]:
        dtrain = xgb.DMatrix(X[idx[0]][:, mask_nota_z], label=Y[:, ii][idx[0]])
    else:
        dtrain = xgb.DMatrix(X[idx[0]], label=Y[:, ii][idx[0]])
        
    bst = xgb.train(
        Final_parameters,
        dtrain,
        num_round,
        evals=[(dtrain, "train")],
        early_stopping_rounds=10,
        verbose_eval=True,
    )

    ###Save Complete model
    bst.save_model("../models/icfes_20142_{0}.model".format(labels_names[ii]))
