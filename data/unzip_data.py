import tarfile
import os

fnames = os.listdir()

try:
    os.mkdir('unziped_data')
except:
    print('unziped_data is already a directory. Saving files there...')

for fname in fnames:
    if fname.endswith("tar.gz"):
        tar = tarfile.open(fname, "r:gz")
        tar.extractall("./unziped_data")
        tar.close()
