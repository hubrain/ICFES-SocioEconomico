import streamlit as st
import pandas as pd
import numpy as np
import altair as alt
from PIL import Image
import os
import sys
from sklearn.metrics import confusion_matrix


def compute_density_and_presicion(
    df,
    puntaje,
    decil,
):
    deciles_puntaje = "DECILE_" + puntaje

    y_real = df["Deciles_{}_top_{}0%".format(deciles_puntaje, decil)].tolist()
    y_pred = df["Deciles_{}_pred_top_{}0%".format(deciles_puntaje, decil)].tolist()
    cm = confusion_matrix(y_real, y_pred, labels=[1, 0])
    presicion = (cm[0, 0] / (cm[0, 0] + cm[0, 1])) * 100
    exactitud = (cm[0, 0] / (cm[0, 0] + cm[1, 0])) * 100

    hist, bin_edges = np.histogram(df[puntaje].dropna(), bins="rice", density=True)
    quantiles = df[puntaje].quantile([ii / 10 for ii in range(1, 10)]).values
    df_graph = pd.DataFrame.from_dict({puntaje: bin_edges[0:-1], "densidad": hist})
    name_new_column = "Respecto al {}0% mejor".format(str(decil))
    df_graph[name_new_column] = np.where(
        df_graph[puntaje] < quantiles[9 - decil], "Debajo", "Encima"
    )
    min_above = min(df_graph[df_graph[name_new_column] == "Encima"][puntaje].values)
    value = min(df_graph[df_graph[puntaje] == min_above]["densidad"].values)
    df_1 = pd.DataFrame.from_dict(
        {puntaje: [min_above], "densidad": [value], name_new_column: ["Debajo"]}
    )
    df_graph = df_graph.append(df_1)

    bars = (
        alt.Chart(df_graph)
        .mark_area()
        .encode(
            x="{}:Q".format(puntaje),
            y="densidad:Q",
            color=name_new_column,
            tooltip=[name_new_column],
        )
    )

    density_plot = (bars).interactive()

    return density_plot, presicion, exactitud


def main(fecha):

    st.markdown(
        """
        # Modelos predictivos
        """
    )

    dict_subject_2018 = {
        "Global": "PUNT_GLOBAL",
        "Matemáticas": "PUNT_MATEMATICAS",
        "Ciencias Naturales": "PUNT_C_NATURALES",
        "Sociales Ciudadanas": "PUNT_SOCIALES_CIUDADANAS",
        "Lectura Crítica": "PUNT_LECTURA_CRITICA",
        "Inglés": "PUNT_INGLES",
    }

    dict_subject_2014 = {
        # "Puesto": "ESTU_PUESTO",
        "Global": "PUNT_GLOBAL",
        "Matemáticas": "PUNT_MATEMATICAS",
        "Ciencias Naturales": "PUNT_C_NATURALES",
        "Sociales Ciudadanas": "PUNT_SOCIALES_CIUDADANAS",
        "Inglés": "PUNT_INGLES",
        "Lectura Crítica": "PUNT_LECTURA_CRITICA",
        "Competencia Ciudadana": "PUNT_COMP_CIUDADANA",
        "Razonamiento cuantitativo": "PUNT_RAZONA_CUANTITATIVO",
    }

    dict_periodo = {
        "2014-2": dict_subject_2014,
        "2018-1": dict_subject_2018,
        "2018-2": dict_subject_2018,
    }

    option_periodo = st.selectbox("¿Qué periodo deseas analizar?", tuple(dict_periodo))

    # Open data with pandas
    dict_name_data_file = {
        "2014-2": "icfes_20142_pred_and_deciles_no_features.csv",
        "2018-1": "icfes_20181_pred_and_deciles_no_features.csv",
        "2018-2": "icfes_20182_pred_and_deciles_no_features.csv",
    }

    currpath = os.path.dirname(os.path.abspath(__file__))
    data_dir = "../../assets/data/"
    data_file_name = data_dir + dict_name_data_file[option_periodo]
    df_puntajes = pd.read_csv(os.path.join(currpath, data_file_name))

    option_subject = st.selectbox(
        "¿Qué materia deseas analizar", tuple(dict_periodo[option_periodo])
    )

    dict_percentiles = {
        "10 %": 1,
        "20 %": 2,
        "30 %": 3,
        "40 %": 4,
        "50 %": 5,
        "60 %": 6,
        "70 %": 7,
        "80 %": 8,
        "90 %": 9,
    }

    option_percentiles = st.selectbox(
        "¿Por encima de qué percentil quieres analizar?", tuple(dict_percentiles)
    )

    density_plot, presicion, exactitud = compute_density_and_presicion(
        df_puntajes,
        dict_periodo[option_periodo][option_subject],
        dict_percentiles[option_percentiles],
    )
    st.markdown(
        "Encima de **{}**.  \n Precisión {:.1f} %  \n Exactitud {:.1f} %".format(
            option_percentiles, presicion, exactitud
        )
    )

    st.altair_chart(density_plot, use_container_width=True)