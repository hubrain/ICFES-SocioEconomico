import streamlit as st
import pandas as pd
import os
import datetime
import sys
from PIL import Image


currpath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(currpath, ".."))

# TODO: campos por agregar al dataframe
# 1) Si el estudiante está por encima o por debajo de la media
# 2) Si el estudiante está el los mejores 10 %, 20 %, etc , esto para una lista de valores
# 3) Si el estudiante está en los peores 10 %, 20 %, etc esto para una lista de valores
# 4) El decil en el cual se encuentra el estudiante
# 5) ...


def main():
    """
    This is the handler of every tab in our application
    """

    from models_graphs import main as models_graphs
    from shap_hiplot_graphs import main as shap_hiplot_graphs

    currpath = os.path.dirname(os.path.abspath(__file__))

    hide_menu_style = """
        <style>
        #MainMenu {visibility: hidden;}
        </style>
        """
    st.markdown(hide_menu_style, unsafe_allow_html=True)

    # Titulo del Dashborad
    st.sidebar.title(
        """
        El desempeño en las pruebas del Icfes Saber 11
    """
    )

    # Hubrain logo
    st.sidebar.image(
        os.path.join(currpath, "../../assets/hubrain_logo.png"), use_column_width=True
    )

    st.sidebar.markdown(
        "### Análisis de las pruebas Saber 11, por [hubrain](http://hubrain.co)"
    )

    st.sidebar.markdown(
        "Esta aplicación utiliza los datos oficiales del Icfes para los periodos **{0}**, **{1}**, **{2}**.".format(
            "2014-2", "2018-1", "2018-2"
        )
    )

    # Option menu
    option = st.sidebar.radio(
        "¿Qué quieres ver?",
        ["Modelo predictivo", "Interpretación del modelo"],
    )

    if option == "Modelo predictivo":
        models_graphs("2020-12-02")
    elif option == "Interpretación del modelo":
        shap_hiplot_graphs("2020-12-02")

    st.sidebar.markdown(
        "Estamos abiertos a colaborar. Nos puedes contactar en <hubraintech@gmail.com>"
    )
    st.sidebar.markdown(
        """
        Miembros del proyecto:\n
        - Nicolás Parra Avila
        - Vladimir Vargas Calderón
        - Juan Sebastián Flórez
        - Leonel Fernando Ardila
        - Oscar Alejandro Quintero

    """
    )

    st.sidebar.markdown(
        """
            Este Sitio web es puesto a disposición por [hubrain](http://hubrain.co) sólo con fines pedagógicos 
            e informativos. 
        """
    )
    hide_footer_style = """
    <style>
    .reportview-container .main footer {visibility: hidden;}
    """
    st.markdown(hide_footer_style, unsafe_allow_html=True)


if __name__ == "__main__":
    main()
