import streamlit as st
import hiplot as hip
import pandas as pd
import altair as alt
from PIL import Image
import sys
import os


currpath = os.path.dirname(os.path.abspath(__file__))


def main(fecha):

    st.markdown(
        """
        # ¿Qué nos dicen los modelos? \n
        """
    )

    # Gráficas de SHAP
    dict_subject_2018 = {
        "Global": "PUNT_GLOBAL",
        "Matemáticas": "PUNT_MATEMATICAS",
        "Ciencias Naturales": "PUNT_C_NATURALES",
        "Sociales Ciudadanas": "PUNT_SOCIALES_CIUDADANAS",
        "Inglés": "PUNT_INGLES",
        "Lectura Crítica": "PUNT_LECTURA_CRITICA",
    }

    dict_subject_2014 = {
        # "Puesto": "ESTU_PUESTO",
        "Global": "PUNT_GLOBAL",
        "Matemáticas": "PUNT_MATEMATICAS",
        "Ciencias Naturales": "PUNT_C_NATURALES",
        "Sociales Ciudadanas": "PUNT_SOCIALES_CIUDADANAS",
        "Inglés": "PUNT_INGLES",
        "Lectura Crítica": "PUNT_LECTURA_CRITICA",
        "Competencia Ciudadana": "PUNT_COMP_CIUDADANA",
        "Razonamiento cuantitativo": "PUNT_RAZONA_CUANTITATIVO",
    }

    dict_periodo = {
        "2014-2": dict_subject_2014,
        "2018-1": dict_subject_2018,
        "2018-2": dict_subject_2018,
    }

    option_periodo = st.selectbox("¿Qué periodo deseas analizar?", tuple(dict_periodo))

    option_subject = st.selectbox(
        "¿Qué materia deseas analizar", tuple(dict_periodo[option_periodo])
    )

    st.markdown("Materia escogida **{0}**".format(option_subject))

    # Graficas de SHAP
    col1, col2 = st.beta_columns(2)
    col1.header("Mililes")
    name_image = os.path.join(
        currpath,
        "../../assets/shap_graphs/{}/DECILE_{}impact_new.png".format(
            option_periodo, dict_periodo[option_periodo][option_subject]
        ),
    )
    deciles = Image.open(name_image)
    col1.image(deciles, use_column_width=True)

    col2.header("Nota Z")
    name_image = os.path.join(
        currpath,
        "../../assets/shap_graphs/{}/NOTA_Z_COLE_CODIGO_ICFES_{}impact_new.png".format(
            option_periodo, dict_periodo[option_periodo][option_subject]
        ),
    )
    nota_z = Image.open(name_image)
    col2.image(nota_z, use_column_width=True)

    """

    x1, x2, x3 = st.slider("x1"), st.slider("x2"), st.slider("x3")

    # Create your experiment as usual
    data = [
        {
            "uid": "a",
            "dropout": 0.1,
            "lr": 0.001,
            "loss": 10.0,
            "optimizer": "SGD",
            "x": x1,
        },
        {
            "uid": "b",
            "dropout": 0.15,
            "lr": 0.01,
            "loss": 3.5,
            "optimizer": "Adam",
            "x": x2,
        },
        {
            "uid": "c",
            "dropout": 0.3,
            "lr": 0.1,
            "loss": 4.5,
            "optimizer": "Adam",
            "x": x3,
        },
    ]
    xp = hip.Experiment.from_iterable(data)

    # Display with `display_st` instead of `display`
    ret_val = xp.display_st(ret="selected_uids", key="hip")

    # st.markdown("hiplot returned " + json.dumps(ret_val))

    """
