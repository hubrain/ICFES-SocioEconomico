# ICFES-SocioEconomico

En este repositorio se crean modelos para predecir los puntajes obtenidos de estudiantes según sus variables socio-económicas.

### Financiación por parte del Icfes

Obtuvimos financiación mediante nuestra participación en la [Convocatoria Estudiantes 2019 ICFES](https://www.icfes.gov.co/convocatorias-de-investigacion) a nombre de Leonel Fernando Ardila Peña (Universidad Nacional de Colombia)

En este repositorio se encuentran los modelos y datos abiertos del ICFES utilizados para estudiar la relación entre el desempeño académico y el estatus socio-económico en las pruebas SABER 11 de los años 2014 y 2018.

### Investigación

Realizamos investigación a profundidad para el período 2018-2. Por este motivo, la carpeta central de este repositorio es la de code/2018-2, y tiene más scripts que los otros períodos que solamente fueron utilizados para la convocatoria del Icfes.

---

## Instrucciones para colaboradores

Dentro de `data` hacer una carpeta: `mkdir unziped_data`, y descomprimir los archivos de `data` dentro de `unziped_data`. Dicha descompresión puede ser hecha por un archivo `unzip_data.py` que está dentro de `data`. Esta última carpeta es ignorada por git, así que no será problemática para push de archivos grandes.
